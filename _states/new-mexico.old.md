---
title: New Mexico
heading: Election Issues in New Mexico
meta_desc: An overview of key election integrity issues in New Mexico during the 2020 US Presidential elections.
number_of_disputed_ballots: Unknown
biden_winning_margin: 99,720 votes (11.0%)
electoral_college_votes: 5 votes
summary: |
   - Late (and illegal) changes to ballot collection methods
battleground_state: true
published: false
---

<!-- THIS CASE SEEMS TO HAVE BEEN WITHDRAWN. Probably not hugely relevant. -->

**Number of disputed ballots** 		| 		{{page.number_of_disputed_ballots}}
**Joe Biden's winning margin**      |       {{page.biden_winning_margin}}
**{{page.title}}'s electoral college votes under dispute**   |       {{page.electoral_college_votes}} <br> <small>(36 would need to be flipped for a Trump win)


*From The American Spectator's article "[Election 2020: Can Classic Fraud Evidence Save The Day?](https://spectator.org/election-2020-can-classic-fraud-evidence-save-the-day/)":*

On December 14 Trump attorneys filed a complaint in federal district court against New Mexico election officials, seeking an injunction pendent lite (“during the litigation”) against state officials, rescinding their certification of New Mexico’s 5 Electors. 

The complaint, as was the case in Wisconsin, is framed as a pure question of law — hence, no fact-finding proceeding is needed. Essentially, Democrat election officials made changes to the law as passed by the legislators, in the guise of interpretation. 

Put simply, before 2019, there were three recognized ways that voters could exercise the franchise by using absentee ballots: 

1. Mailing them in; 
2. Walking them in to the county clerk’s office; or 
3. Walking them into a polling place

In 2019, New Mexico election officials added a fourth way by which to vote absentee: voting by “secured container.” These containers were to be monitored by a video surveillance system and were to be ready at least 90 days before the general election. New Mexico’s top election official extended this idea to include drop boxes, a term not found in the law. As of this writing, the court has not responded.



{% comment %}
### Footnotes & References

[^1]: The American Spectator, [Election 2020: Can Classic Fraud Evidence Save The Day?](https://spectator.org/election-2020-can-classic-fraud-evidence-save-the-day/), John C. Wohlstetter, January 4, 2021
{% endcomment %}