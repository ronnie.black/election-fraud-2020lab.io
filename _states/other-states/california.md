---
title: California
heading: Election Fraud in California
state: California
abbr: ca
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}


## Election Integrity Report

In March 2021, a team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report *"2020 Presidential Election Contrast Analysis"* which lists California as the state with the highest number of unexpected Biden votes (by a huge margin).

[Read the PDF Report](https://election-integrity.info/Contrast_Report.pdf){:.button}


{% include seth_state_page_summary %}

{% include electronic_voting_machines %}


## 2021 Governor Recall Election

During this election on September 14, there were numerous issues reported, including:

 1. Voters turning up at polling places and being told they had already voted [^3]

 2. Torrance Police recovered more than 300 unopened California recall election ballots inside the vehicle of a suspect who was found passed out in his car, along with a gun and drugs [^7]

 2. Holes in ballot envelopes that allowed someone to see who the vote was for [^4]

 3. The ability to shine a torch through the ballot envelope and see who the vote was for [^5]

 4. 400,000 votes *disappeared* from the counts during CNN's live coverage [^1]

 5. Mail-in ballots being sent to people who no longer live in California and multiple ballots being sent to people [^6]

 Analyst Draza Smith also reported mathematical patterns in the ballot counts that, like the 2020 election, appear to demonstrate the presence of computer algorithms controlling and manipulating the results. [Read her initial reports](/draza-reports/california-recall-election/).

 California voters are encouraged to verify the status of their recent votes via the [California Secretary of State Voter Status Page](https://voterstatus.sos.ca.gov/) and also via [BallotTrax](https://california.ballottrax.net/voter/) and immediately report any irregularities.


{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

### Footnotes & References

[^1]: Rumble.com Video: [350,000 California Recall Votes Vanish on CNN](https://rumble.com/vmjoc1-350000-california-recall-votes-vanish-on-cnn.html), Sep 15, 2021

[^2]: Rumble.com Video from OAN News: [California Residents Experience Voting Issues Ahead of Governor Recall Election](https://rumble.com/vmfd5o-californias-already-screwing-with-the-ballots-nothing-to-see-here..html)

[^3]: This happened at at least two locations in Woodland Hills, California. The Los Angeles County Registrar-Recorder's office blamed the problem on electronic poll book devices used to check in voters.

    - El Camino Real Charter High School, Woodland Hills. Reported by KTLA News and the Gateway Pundit: [Republicans in SoCal Have Trouble Casting Recall Ballots – Many Told ‘They Already Voted’ – Even Though They Had Not](https://www.thegatewaypundit.com/2021/09/republicans-socal-trouble-casting-recall-ballots-many-told-already-voted-even-though-not/), Sep 12, 2021. 
    
    - Disabled American Veterans Hall, 6543 Corbin Ave, Woodland Hills, [see Telegram post](https://t.me/KanekoaTheGreat/1670), [video testimony on Rumble](https://rumble.com/vmil62-california-recall-election-woodland-hills-polling-location-70-of-voters-tol.html), and [coverage on Fox 11 News](https://rumble.com/vmilw6-fox11news-california-recall-voters-told-they-already-voted-in-woodland-hill.html).

[^4]: Demonstrated in this Rumble.com Video: [Gavin Newsom Cheating In Recall Election With Votes Visible Through Envelope](https://rumble.com/vlfpob-gavin-newsom-cheating-in-recall-election-with-votes-visible-through-envelop.html), uploaded Aug 20, 2021.

[^5]: Shown in a photograph in this Telegram post: <https://t.me/ladydraza/777>

[^6]: Reported by Twitter users, [here](https://twitter.com/libsoftiktok/status/1437418814992486400?s=19). Note that these claims have not been verified.

[^7]: Fox 11 News Los Angeles: "[Over 300 California recall election ballots discovered in suspect's vehicle alongside gun, drugs](https://www.foxla.com/news/over-300-california-recall-election-ballots-discovered-in-suspects-vehicle-alongside-gun-drugs)", Aug 23, 2021.