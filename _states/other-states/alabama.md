---
title: Alabama
heading: Election Fraud in Alabama
state: Alabama
abbr: al
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}