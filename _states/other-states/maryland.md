---
title: Maryland
heading: Election Fraud in Maryland
state: Maryland
abbr: md
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Maryland as the state with the 5th highest number of unexpected Biden votes

* ElectionFraud20.org has also reviewed the voting and registration trends from 2000 to 2020 and noted several anomalies in both the voter rolls and the ballot counts in Maryland, among others. [Read our Maryland Case Study](/trend-analysis/registration-analysis/maryland/) and the preceding articles that explain the method and the observations.

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}