---
title: Kentucky
heading: Election Fraud in Kentucky
state: Kentucky
abbr: ky
last_updated: 6 Dec 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

Dr. Douglas Frank reports that he had significant trouble getting access to the specific 2020 election data he needed for his analysis, despite both parties having access to it:

  *"Hmmm..... What does that tell you about the parties? If they aren't helping, they are complicit. What is sad and shocking to me is how many in the establishment know what is going on, but look the other way. They expect us... the citizens... to do their work."*  
  <small>Source: [Telegram post](https://t.me/FollowTheData/950), Oct 3, 2021</small>
