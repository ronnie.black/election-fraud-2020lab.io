---
title: Massachusetts
heading: Election Fraud in Massachusetts
state: Massachusetts
abbr: ma
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Massachusetts as the state with the 2nd highest number of unexpected Biden votes

* Dr. Shiva Ayyadurai ran for a Massachusetts US Senate seat in 2020, and believes he witnessed fraud in the process. He also describes the retaliation directed at him after he questioned the State Election Director of Massachusetts as to why Massachusetts had violated 52 USC 20701 by deleting  ballot images. See his [response to Maricopa County](https://www.scribd.com/document/530454189/Dr-Shiva-Replies-To-Maricopa-County-Officials) (p.22).

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}