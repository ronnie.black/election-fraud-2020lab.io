---
title: Ohio
heading: Election Fraud in Ohio
state: Ohio
abbr: oh
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Ohio

* Back in 2000, computer programmer Clinton Eugene Curtis testified under oath in front of the US House Judiciary in Ohio that he was hired by authorities to help rig the outcome of US elections. In [this video](https://rumble.com/vn1b2j-computer-programmer-testifies-about-rigging-elections-with-voting-software.html?mref=6zof&mrefc=3) from 2000, he explains how he was hired by Congressman Tom Freeny to build prototype software that would allow authorities to push the results to a 51/49 outcome if needed. There is evidence that a similar but more advanced version of this technique was used in 2016 and 2020.

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}