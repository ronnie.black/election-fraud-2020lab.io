---
title: Maine
heading: Election Fraud in Maine
state: Maine
abbr: me
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Maine

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}