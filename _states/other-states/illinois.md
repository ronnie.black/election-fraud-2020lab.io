---
title: Illinois
heading: Election Fraud in Illinois
state: Illinois
abbr: il
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Illinois

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which discusses how Illinois unexpectedly gained a significant number of overall votes, despite a decrease in population

* Illinois Leaks reports that DuPage County signed a contract with Dominion Systems which includes a clause that they will resist all Freedom of Information (FOIA) requests, where possible, and inform Dominion of all such occurrences. [Read their report](https://edgarcountywatchdogs.com/2021/01/dupage-county-clerk-signed-anti-transparency-contract-with-dominion-voting-systems-dominion-encouraged-county-to-resist-disclosure-of-information/).


{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}