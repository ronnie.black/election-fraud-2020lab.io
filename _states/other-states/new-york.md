---
title: New York
heading: Election Fraud in New York
state: New York
abbr: ny
last_updated: 7 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists New York as the state with the 4th highest number of unexpected Biden votes, despite a decrease in population

  * "[Statistical Voting Analysis of the 2020 NY-22 Congressional Contest](https://election-integrity.info/NY-22nd-2020-Report.pdf)"

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}