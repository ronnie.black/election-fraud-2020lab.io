---
title: Kansas
heading: Election Fraud in Kansas
state: Kansas
abbr: ks
last_updated: 23 Nov 2021
---

{% include state_stub_1 %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=0.97
    extra1=nil
    registrationsOver100=true
    exampleCounty=nil
    exampleDate=nil
%}

{% include electronic_voting_machines %}

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}