---
title: Florida Election Analysis by Seth Keshel
state: Florida
abbr: fl
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)



{% include seth_state_details.html %}

## Summary

Trump is up a staggering 1,040,000 votes from 2016, more than double his gain over Romney.

Biden is up 792k, a Democrat record in Florida as the state drifts further right.

Bottom Line: Biden appears at least 296k votes heavy based on registration trends and historical data tracking with population increases.  If accurate, counting only excess votes, Trump’s margin is FL is more accurately represented at 52.6% to 46.4%, or a 6.2% margin of victory, and margin of 667k votes.

Does that seem too high?  It shouldn’t – these are typical margins for someone like Marco Rubio, who gets Trumpian numbers with Cuban and Venezuelan voters and pours it on in the red counties.

**Best targets for audits:** Lake, Pasco, Santa Rosa, Volusia, Walton.

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)


<small>Source: [Telegram post](https://t.me/CaptainKMapsandStats/41)</small>


{% include seth_worst_counties_for_state heading=true %}


## Tampa, Hillsborough County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vixyek/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<small>Source: [Telegram post](https://t.me/RealSKeshel/1031)</small>

## Lake County

According to Seth, Lake County, Florida, is showing some movement towards an audit of the election. See [his Telegram post](https://t.me/RealSKeshel/1162) from Aug 31, 2021.


## Palm Beach County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkokkt/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<small>Source: [Telegram post](https://t.me/RealSKeshel/1583), Oct 2, 2021</small>


## Pasco, Volusia, and Lake Counties

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkoncj/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<small>Source: [Telegram post](https://t.me/RealSKeshel/1592), Oct 2, 2021</small>


## Seth's Predictions vs Actual Results

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/2260" data-width="100%"></script>


## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like **Florida** and [Texas](../texas/). [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and [Oregon](../oregon/). [Pennsylvania](../pennsylvania/) and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

<small>Source: <https://t.me/RealSKeshel/788></small>


{% include seth_reports_methodology %}

{% include raw_data %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Florida

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which includes a section on {{ page.state }}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
