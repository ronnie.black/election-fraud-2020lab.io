---
title: Minnesota Election Analysis by Seth Keshel
state: Minnesota
abbr: mn
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vht0nv/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{% include seth_state_details.html %}

Trump is up 161k votes from 2016 as the state nearly dropped into the GOP column, but Biden is up 349k after two consecutive elections of fewer votes, including Clinton down 178k from Obama’s re-election.  They will gain votes in 2020 thanks to third parties returning.

Fair share of green counties here, but there are a ton of rural, tiny counties with little play in numbers.  Hennepin is a cesspool, and Ramsey a smaller version.  Biden is 2x Obama’s high turnout era record gain, plus another 30k.  I’ve afforded him Obama ’08 gain plus 25%, same as Ramsey, almost certainly high.  

if Biden is 228k heavy, Trump would be within 5k votes, at 0.1% margin.  I suspect I’m way light on Hennepin, and believe this was a 1-3 point Trump state.  Ellison told you at 4 PM on 11/3 he didn’t have the votes.

Best for audits – Anoka (12k), Carver (8k), Scott (9k), Wright 10k.

<small>Source: <https://t.me/CaptainKMapsandStats/83></small>

----

{% include seth_worst_counties_for_state heading=true %}

### Further Points

* Minnesota resembles [Wisconsin](../wisconsin/); [Pennsylvania](../pennsylvania/)/[Michigan](../michigan/) resemble each other. Third party vote very swingy coming back to two parties in Minnesota.

* I have Trump down to the wire here, with Biden well over what I would call 200k excess expected votes. 

* I have been generous to Biden, allowing 125% of Obama’s historic high turnout era (2004-present) gains in Hennepin and Ramsey.

* St. Louis County is way too high for Biden under recent trends, enough for local Dem mayors to endorse Trump in Iron Range. 

* AG Ellison tweeted late in afternoon of 11/3 they didn’t have the votes to win the state, just before they won the Obama-Romney race margin, after Trump spent big time there and campaigned?

* [Wisconsin](../wisconsin/) and Minnesota were 7-8% Obama states in 2012 and Minnesota 2.3% left of Wisconsin in 2016. I have Wisconsin to Trump at 3.7%. I’m sure I’m light in Twin Cities and would expect it to tilt to Trump up to as much as 2.5%. 

* Lewis won the Senate seat with numbers like this.

<small>Source: <https://t.me/RealSKeshel/512></small>


### Additional Reports

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/565"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="RealSKeshel/859" data-width="100%"></script>

![](/images/seth-keshel/mn-tweet.png)


{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Minnesota

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Minnesota as the state with the 9th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include state_telegram_channels %}
