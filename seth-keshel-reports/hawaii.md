---
title: Hawaii Election Analysis by Seth Keshel
state: Hawaii
abbr: hi
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


I remember a recent president from Hawaii who maxed out at 325k votes, with GOP topping out at 121k, as main voting center of state only grew by 2% in population over the entire decade.  It seems people had been waiting for Biden this entire time.

Trump’s gain of 68k in Hawaii is a GOP record.  Hawaii added 76k new voters since 2016, and when viewing the trend, there has been a tightening since 2008, with loss of D votes all around since Obama’s first run.  Historically, GOP incumbents run stronger in Hawaii during re-election (Reagan won it and George W. Bush had 45%).

Estimated excess:

Hawaii | 10k
Maui | 10k
Kauai | 5k
Honolulu | 30k

If told ahead of time Trump would receive 197k votes, I am estimating that Biden is 55k higher than would be thought possible, at most generous expectations and assigning all third party totals from 2016 to his column.  More accurate margin considering ONLY excess votes is about 21.9%, with Trump near 40% in Honolulu.

All should be audited.


<small>Source: <https://t.me/CaptainKMapsandStats/8></small>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which discusses how Hawaii unexpectedly gained a significant number of overall votes, despite a decrease in population

{% include canvass/get-involved %}

{% include state_telegram_channels %}
