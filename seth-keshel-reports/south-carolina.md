---
title: South Carolina Election Analysis by Seth Keshel
state: South Carolina
abbr: sc
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

I knew there were some issues in South Carolina, but didn’t think they were so widespread.  

Even allowing population and vote expansion, Biden appears to have an unexpected excess of 119,000 votes, with only 22 of 46 counties trending "clean" (meaning they align with registration trends).  A massive coastal operation seems to have brought Biden a +236k gain over Clinton, blowing away Obama’s 2008 mark of 200k gained.  Trump was also up in the state 230k, and it appears he won it by a mile.

This state has lots of target rich GOP counties to go after for audits.

**Worst counties:**

Charleston, Greenville, Horry, Spartanburg | 12k
Berkeley, York, Richland | 8k

If Biden is 119k heavy, an accurate Trump margin is 17.3%, or 57.9% to 40.6%, and 412k votes.

Best GOP county audit targets: Beaufort, Berkeley, Dorchester, Greenville, Horr, Lexington, Spartanburg, York

Most important to audit: Charleston


<small>Source: <https://t.me/CaptainKMapsandStats/103></small>

----


{% include seth_worst_counties_for_state heading=true %}


## Seth's Presentation in Beaufort, South Carolina

{:.small}
Monday, August 30, 2020. 49min runtime.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vj9zir/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}

## See Also

**Lynz Piper-Loomis** is running for South Carolina's 1st Congressional District. She has pledged strong support to face election fraud. Find her at [LynzSC.com](https://www.lynzsc.com/) or on Telegram at [@LynzPiperLoomis](https://t.me/lynzpiperloomis).