---
title: Tennessee Election Analysis by Seth Keshel
state: Tennessee
abbr: tn
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Very troubling map of Tennessee.  

Trump up 330k, a GOP record even above the low turnout '00 election to '04.  Obama +51k from Kerry, down 127k, Clinton down 90k, even with MASSIVE GROWTH in Tennessee.  Now Biden is up 273k, with at least 101k excess.  State was a wipeout, even giving Biden a modern record vote gain for Democrats.

Shelby (Memphis) isn’t too far from trend.  Davidson should have huge Democrat growth and does, flagging it for 15k excess (lenient).  Previous high gain for Democrats is 16k; Clinton up 5k, and that came with 13k Republican loss in 2016.  Trump is up 16k, with Biden up 51k – though I’d expect a large surge there.

The most significant areas are in proximity to Davidson, with Knox being another spot with some spread.  Hamilton is obvious, at least 10k heavy.

Best Trump county audit targets: Hamilton, Knox, Rutherford.

If Biden is 101k heavy, Trump margin would be 62.7% to 35.3%, or 809k.


<small>Source: <https://t.me/CaptainKMapsandStats/118></small>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
