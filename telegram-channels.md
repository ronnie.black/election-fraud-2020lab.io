---
title: List of Election Audit Channels on Telegram
meta_desc: A detailed list of Telegram channels pursuing election integrity, following 2020 Presidential election.
last_updated: 15 Nov 2021
comment_intro: Know of other reputable election integrity channels? Let us know in the comments below.
---

Join forces with others in your region who are working towards election integrity. Because of the heavy censorship occurring on Twitter and Facebook, most groups have moved to Telegram.

Download the app at [Telegram.org](https://telegram.org/).

Here's a list of the Telegram channels we know about so far that are working together for election audits and other integrity measures:

{:.warning.emoji}
⚠️ **WARNING:**  
We've received reports that *some* channels connected with "America First Audits" (though possibly not all of these), including channels led by "Salty Lulu" have become paranoid, overly controlling, and in some cases have obstructed the constructive work done by many volunteers in the election integrity movement. If you encounter this, please move on and find other groups doing effective work, and please report these channels in the comments section below so that we can filter them out.

{:.info.emoji}
ℹ️ You can also volunteer with USEIP which has grassroots groups working across many states, although they don't use Telegram for collaboration. Visit [USEIP.org](https://useip.org/get-involved/) to sign up.


<table>
{% for channels in site.data.telegramAuditChannels %}
    {% assign stateData = site.data.states | find: 'abbr_lowercase', channels[0] %}
    <tr>
        <th>
            {% if stateData %}
                {{ stateData.name }}
            {% else %}
                Nation-Wide
            {% endif %}
        </th>
        <td>
            {% for channel in channels[1] %}
                {% unless forloop.first %}
                    <br>
                {% endunless %}
                {% include telegram_link channel=channel %}
            {% endfor %}
        </td>
    </tr>
{% endfor %}
</table>

If there's no Audit Force for your state and you would like to get plugged in, join the [Audit Force State Requests Discussion](https://t.me/joinchat/lyijRUjHark1NmQx).


### Other Election Integrity Channels

[@AuditWatch](https://t.me/AuditWatch)  
[@theprofessorsrecord](https://t.me/theprofessorsrecord) (Professor David K. Clements)  
[@FollowTheData](https://t.me/FollowTheData) (Dr. Doug Frank)  
[@RealSKeshel](https://t.me/RealSKeshel) (Seth Keshel)  
[@electiondataanalyzer](https://t.me/electiondataanalyzer) (a tool for analyzing voter registration anomalies)  
[@JovanHuttonPulitzer](https://t.me/JovanHuttonPulitzer) (Jovan Pulitzer)  
[@praying_medic](https://t.me/praying_medic)  
[@joeoltmann](https://t.me/joeoltmann) (Joe Oltmann)  
[@maninamerica](https://t.me/maninamerica)  
[@drawandstrikechannel](https://t.me/drawandstrikechannel)  
[@CodeMonkeyZ](https://t.me/CodeMonkeyZ) (Ron Watkins)  
[@electionevidence](https://t.me/electionevidence)  
[@KanekoaTheGreat](https://t.me/KanekoaTheGreat)  

And last, but not least, our own channel [@ElectionFraud20_org](https://t.me/ElectionFraud20_org)

