---
title: North Carolina Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 23 Nov 2021
state: North Carolina
list_of_counties:
---

{% include dr_frank_state_report %}

## Charts For Each County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjn8b5/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

