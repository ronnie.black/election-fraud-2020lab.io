---
title: Montana Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 23 Nov 2021
state: Montana
list_of_counties:
  - Beaverhead
  - Big Horn
  - Blaine
  - Broadwater
  - Carbon
  - Carter
  - Cascade
  - Chouteau
  - Custer
  - Daniels
  - Dawson
  - Deer Lodge
  - Fallon
  - Fergus
  - Flathead
  - Gallatin
  - Garfield
  - Glacier
  - Golden Valley
  - Granite
  - Hill
  - Jefferson
  - Judith Basin
  - Lake
  - Lewis and Clark
  - Liberty
  - Lincoln
  - Madison
  - McCone
  - Meagher
  - Mineral
  - Missoula
  - Musselshell
  - Park
  - Petroleum
  - Phillips
  - Pondera
  - Powder River
  - Powell
  - Prairie
  - Ravalli
  - Richland
  - Roosevelt
  - Rosebud
  - Sanders
  - Sheridan
  - Silver Bow
  - Stillwater
  - Sweet Grass
  - Teton
  - Toole
  - Treasure
  - Valley
  - Wheatland
  - Wibaux
  - Yellowstone

---

{% include dr_frank_state_report %}


## Counties with High/Low Correlations

![](/images/dr-frank/montana/correlations.jpg)



{% for county in page.list_of_counties %}

## {{ county }} County

![{{ county }} County 2020 Election Analysis Chart by Dr. Doug Frank](/images/dr-frank/montana/{{ county | slugify }}.jpg)

----

{% endfor %}

