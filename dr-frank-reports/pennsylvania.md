---
title: Pennsylvania Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 23 Nov 2021
state: Pennsylvania
list_of_counties:
---

{% include dr_frank_state_report %}

## Charts For Each County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vlsq0l/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

## Full Presentation

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhgztu/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>