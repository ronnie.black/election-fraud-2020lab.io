---
title: Dr. Douglas G. Frank’s Election Analysis
last_updated: 23 Nov 2021
breadcrumb: "[Follow the Data](#colophon) ›"
meta_desc: An overview of Dr. Douglas Frank's analysis of the 2020 election, and the anomalies he found.
comment_intro: What are your thoughts on Dr. Frank's analysis? Or have you found additional materials related to his work that would benefit others? Let us know in the comments below.
---

{:.small}
*Introductory material sourced from [Montana Election Integrity Project](https://www.mtelectionintegrity.com/news/40-year-elections-modeler-predicted-outcome-of-53-of-56-montana-county-elections).*

Dr. Douglas G. Frank is a 40-year modeler of pandemics and elections, a renowned scientist, and inventor who received his PhD in Electrochemistry at UC Santa Barbara. He has authored approximately sixty peer-reviewed scientific reports, including feature and cover articles in the leading scientific journals in the world (Science, Nature, Naturwissenschaften). 

Dr. Frank built upon Kurt Hyde’s theory from 2010 that suggested that US Census data could be used to artificially inflate voter registration databases across the country. He further asserted that voter registration databases were indeed inflated during the 2020 General Election and that, using an algorithm that he theorizes was developed at the state level and applied to the voter registration information in each individual county in a state, the voter turnout of the election was predictable to an unusually high level of accuracy.

Using 2010 US Census data and updated 2019 American Community Survey data from the Census Bureau, Dr. Frank is able to predict not only the voter turnout by age, but voter registration by age. When Frank’s prediction is compared to the actual voter turnout and voter registration by age, it correlates to an unusually high level of accuracy. In the case of Missoula County, and most other of the 55 other counties in Montana, his prediction was correlative to 0.99.

![Dr Frank Missoula, Montana](/images/dr-frank/montana/missoula.jpg)

{:.small}
Missoula County election results analysis. Dr. Douglas G. Frank. September 21, 2021.  
**Blue Line:** Population  
**Black Line:** Registered Voters  
**Red Line:** Ballots cast  
**Light Blue Line:** Predicted ballots based on US Census Bureau data combined with Dr. Frank's algorithm

Notice how closely the red and light blue lines are in alignment.

Counties across Montana had more registered voters in certain age groups than eligible voters exist in the population, and in many cases in older cohorts, voter turnout was nearly 100% and sometimes exceeded the total eligible population. 

More astonishingly, the exact same algorithm (or “key” as Frank refers to the mathematical formula) used in Missoula County could be applied to any other county in Montana (except for Petroleum, Treasure, and Wibaux counties) to predict their voter registration and turnout by age, too. See the [detailed charts for all of Montana](montana/).

Dr. Frank has performed his analysis on at least 14 other states and has found the same disturbing pattern where he is able to predict the voter turnout rate for every age bracket in many counties based only US Census Bureau data. <!-- and registration numbers too, possibly? -->

The following chart shows an overview of each state and how "clean" or "dirty" their voter rolls and turnout numbers appear to be:

![Dr. Frank National Overview Chart](/images/dr-frank/all-states.jpg)

{:.small}
Source: [Telegram Post](https://t.me/FollowTheData/1120), Nov 21, 2021


## Findings By State

### Detailed Charts

<!-- Dr. Frank was able to predict voter turnout by age and voter registration by age with a very high degree of accuracy (a correlation of 0.996). He also found that counties across Montana had more registered voters in certain age groups than eligible voters in the population. Dr. Frank shared his analysis of all 56 Montana counties at the Montana Election Integrity livestream and in-person event Take Back Montana: 2020 Election Analysis. -->

<div style="column-width: 8em" markdown=1>
[Colorado](colorado/)  
[Kansas](kansas/)  
[Montana](montana/)  
[North Carolina](north-carolina/)  
[Pennsylvania](pennsylvania/)  
[West Virginia](west-virginia/)
</div>

### Detailed Videos

<div style="column-width: 8em" markdown=1>
[Arizona](https://rumble.com/vn5hs5-arizona-november-2020-election-analysis.html)  
[Michigan](https://rumble.com/vhy4ml-dr-franks-preliminary-analysis-of-michigans-election-data.html)  
[Ohio](https://rumble.com/vk36mg-dr-frank-in-wellington-ohio.html)  
[Pennsylvania](https://rumble.com/vk35z4-pennsylvania-voter-demographics.html)  
[Wisconsin](https://rumble.com/vk35dj-wisconsin-election-demographics-investigation.html)
</div>

### Animated Charts

<div style="column-width: 8em" markdown=1>
[Alabama](https://rumble.com/vn779z-the-registration-key-to-alabama.html)   
[Colorado](https://rumble.com/vmx2h0-the-registration-key-to-colorado.html)   
[Idaho](https://rumble.com/vk3x7o-idaho-registration-key.html)   
[Indiana](https://rumble.com/vn765z-the-registration-key-for-indiana.html)   
[Kansas](https://rumble.com/vojdit-the-registration-key-for-kansas.html)   
[Missouri](https://rumble.com/vkqt2o-the-registration-key-for-missouri.html)   
[Nebraska](https://rumble.com/vka3v8-nebraska-registration-key.html)   
[North Carolina](https://rumble.com/vm9eg5-the-registration-key-to-north-carolina.html)   
[Ohio](https://rumble.com/vk369e-ohio-voter-demographics.html)   
[Pennsylvania](https://rumble.com/voew51-the-registration-key-for-pennsylvania.html)   
[South Carolina](https://rumble.com/vnnjxd-the-registration-key-for-south-carolina.html)   
[Utah](https://rumble.com/vncps5-the-registration-key-to-utah.html)   
[Washington](https://rumble.com/vk3626-the-registration-key-for-washington-state.html)   
[West Virginia](https://rumble.com/vnfwfn-the-registration-key-to-west-virginia.html)   
</div>

## Written Reports

* [Nine Michigan Counties](https://www.scribd.com/document/534306821/02-Dr-Frank-1-9-Michigan-Counties-040621)
* [Antrim County, Michigan Voting Precinct Demographics](https://www.scribd.com/document/534306371/06-Dr-Frank-2-Antrim-Precincts-042621)


## Video Presentations

The simplest possible explanation of his findings is covered in this 3min video clip *A Simple Metaphor*:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhh0s0/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

A more detailed overview of Dr. Frank's work can be seen in his 1-hour interview with Mike Lindell, *Scientific Proof*:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vcmcjz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

He also presented his findings at Lindell's Cyber Symposium, Aug 10--12, 2021 (which we [covered in another article here](/in-detail/cyber-symposium-mike-lindell/)):

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid7gk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


### Other Presentations

* [Truth & Liberty Coalition Livecast: Dr. Douglas G. Frank on Analyzing the 2020 Election](https://truthandliberty.net/episode/dr-douglas-g-frank-on-analyzing-the-2020-election/), May 31, 2021

* [Truth & Liberty Coalition Livecast: Dr. Douglas G. Frank on Following the Election Data to Find Evidence](https://truthandliberty.net/episode/dr-douglas-g-frank-on-following-the-election-data-to-find-evidence/), Aug 2, 2021

* [Take Back Montana: 2020 Election Analysis](https://www.youtube.com/watch?v=Fc8E3_Qvl6o) --- featuring Captain Seth Keshel, Professor David Clements, and Dr. Douglas Frank (from 1hr 53min mark), Sep 25, 2021


## Supreme Court Case

Dr. Douglas Frank publicly confirmed  that he is contributing a significant amount of research and data into an upcoming Supreme Court case. In a [video interview with Pete Santilli](https://rumble.com/vp4i7h-dr.-frank-epic-interview-with-pete-santilli-november-12-2021.html) (from 21min mark) he gives more background to the case.

He also suggests that the much-discussed packet captures (PCAPs) that Mike Lindell has been in possession of were actually captured using government (CISA) infrastructure, which is why they were not able to be legally released to the public previously. But they will be included in this upcoming case on November 23.

<div class="info" markdown=1>
#### Get Involved

The upcoming Supreme Court case needs Republican State Attorneys General to sign on as litigants. Let your state representatives know that you want them to support this action.
</div>


## Follow Dr. Frank's Updates

<iframe id="preview" style="border:0px;height:500px;width:500px;margin:5px;box-shadow: 0 0 16px 3px rgba(0,0,0,.2);" src="https://xn--r1a.website/s/FollowTheData"></iframe>

