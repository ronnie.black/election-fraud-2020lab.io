---
title: Door-to-Door Canvassing Shows Alarming Results in Seven States
meta_desc: We explain the process of canvassing voters, door-to-door, and present the seven states that are already showing alarming results.
parent_page: /canvassing/overview/
last_updated: 8 Oct 2021
comment_intro: Do you know of other canvassing efforts going on around the country? Or know of details we might have missed? Let us know in the comments below.
states_with_results_below:
  - Arizona
  - Colorado
  - Georgia
  - Florida
  - Michigan
  - Washington
  - Delaware
---

{:.small.muted}
Last updated {{ page.last_updated }}

{:.info}
For an introduction into canvassing and how it works, see *[Grassroots Canvassing Efforts](/canvassing/overview/)*.

Thanks to the diligent canvassing efforts in the following states, alarming anomalies and irregularities are being uncovered. Here are the states that have reported at least some preliminary results from their canvassing of voters after the 2020 election.

{% include toc %}

### Arizona

{% include canvass/arizona %}

[Read the full report on Arizona](/fraud-summary-by-state/arizona/){:.button}

### Colorado

{% include canvass/colorado %}

[Read the full report on Colorado](/fraud-summary-by-state/colorado/){:.button}


### Georgia

{% include canvass/georgia-braynard %}

[Read the full report on Georgia](/fraud-summary-by-state/georgia/){:.button}


### Florida

{% include canvass/florida %}

[Read the full report on Florida](/fraud-summary-by-state/florida/){:.button}


### Michigan

{% include canvass/michigan %}

[Read the full report on Michigan](/fraud-summary-by-state/michigan/){:.button}


### Washington

{% include canvass/washington %}

[Read the full report on Washington](/fraud-summary-by-state/washington/){:.button}


### Delaware

{% include canvass/delaware %}

[Read the full report on Delaware](/fraud-summary-by-state/delaware/){:.button}


### More to Come

*If you're aware of other states that are canvassing, let us know in the comments below.*


<!-- Other states:

    - Texas (Audit Force Channel?)
    - North Carolina (Audit Force Channel)
    - Pennsylvania (Don't Tread Pennsylvania)
      https://t.me/DontTreadPennsylvania/14461
      https://t.me/auditthevotepa      
    - Missouri
    - Not sure about Georgia yet
    - Nevada is training teams, in preparation for canvassing.

    (These should be added to Telegram list, also.)

-->

{% assign canvassPage = site.pages | find: 'url', '/canvassing/get-involved/' %}
{% assign numberOfStates = canvassPage.canvass_teams_by_state | size %}

<div class="info" markdown="1">
## Get Involved

Canvassing is a great way to verify whether voting records are accurate and free of fraud. There are already teams canvassing in at least {{ numberOfStates }} states, and volunteers are needed.

[Learn more](/canvassing/get-involved/){:.button}
</div>


### Footnotes & References
