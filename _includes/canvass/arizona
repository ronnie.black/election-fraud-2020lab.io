The original scope of the Maricopa Forensic Audit included voter canvassing — directly interviewing voters and visiting registered addresses to confirm that official records match how residents actually voted. The Federal Department of Justice swiftly enacted significant pressure against this canvassing, saying that it equated to “voter intimidation”, and would be potentially illegal. This blockade of the Senate’s efforts forced them to focus on the remaining aspects of the audit.

But hundreds of private citizens such as those led by [Liz Harris](https://t.me/VoteLizHarris) had already begun their own grass-roots canvassing effort, visiting 11,708 residential properties and gathering data on 4,570 voters. On Sep 8, 2021, Harris shared [her report](https://crimeofthecentury2020.com/wp-content/uploads/2021/09/Maricopa-County-Canvass-Report-Final-090821.pdf) revealing that:

* **Votes lost**  
Out of 964 residents interviewed who were on record as having *not* voted — over 34% of them said they actually *did* vote. 1 in 3 of these voters had their votes “lost”. If this rate is applied to the entire county, this would account for 165,518--180,690 lost votes. A similar survey in Nov 2020 found an even higher rate of 50.1% of mail-in ballots not being recorded. [^c1]

* **Votes added**  
5.66% of the interviews identified “phantom” voters who did not reside at their specified address, yet submitted mail-in ballots. This could affect an estimated 96,389 mail-in ballots out of the 1.7 million total. They discovered repeated instances over 8+ years whereby this occurred.

  In some cases the ballot was crossed out and returned to the county, at other times the ballot was retained by the real resident; yet in *both* of these types of cases, ballots were somehow recorded as “returned”, and a vote was counted.

* Smaller scale issues such as votes cast by mail from vacant lots

The team is also surveying all Arizona residents via a form at [canvass50.com](https://canvass50.com/), asking as many residents as possible to identify which method they used to vote during the 2020 election, so that any issues with the county records can be identified.


[^c1]: Interestingly, Maricopa County stated in their 2020 Voter Education report that 2,160,412 residents requested an early ballot, but only 1,915,487 of them returned their ballot. This equates to 244,925 voters who were recorded as not returning the ballot that they, themselves requested. Is it logical to expect that 11.3% of engaged voters who requested a ballot decided not to return it?

    For an overview of the Maricopa Voter Education report, see [Dr. Shiva Liva: Maricopa Audit Open Forum for Public, Press & Maricopa Election Officials](https://youtu.be/bF4nMoX0IkY?t=1301), from 21min 40sec mark.

