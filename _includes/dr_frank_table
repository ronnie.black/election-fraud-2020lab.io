
{% if numSortableTablesOnPage %}
    {% assign numSortableTablesOnPage = numSortableTablesOnPage | plus: 1 %}
{% else %}
    {% include vue %}
    {% assign numSortableTablesOnPage = 1 %}
{% endif %}

<div id="sortableTable{{ numSortableTablesOnPage }}" class="sortableTable responsive-table">

{% raw %}
    <table class="small reduce-cell-padding-on-mobile">
        <thead>
            <tr>
                <th v-on:click="sortBy('County')">County</th>
                <th v-on:click="sortBy('% Registered')">Registered Voters vs Residents Over Age 18</th>
                <th v-on:click="sortBy('% Turnout')">% of Registered Voters Who Voted</th>
                <th v-on:click="sortBy('Predictability')">Predictability</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="row in data">
                <td>
                    <a v-if="enableAnchorLinks" v-bind:href="'#' + slug(row['County'])">{{ row['County'] }}</a>
                    <span v-else>{{ row['County'] }}</span>
                </td>
                <td v-bind:class="{red: parseFloat(row['% Registered']) > 95 }">
                    {{ row['% Registered'] }}
                </td>
                <td v-bind:class="{red: parseFloat(row['% Turnout']) > 95 }">
                    {{ row['% Turnout'] }}
                </td>
                <td v-bind:class="{red: parseFloat(row['Predictability']) > 0.95 }">
                    {{ row['Predictability'] }}
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr style="font-weight: bold">
                <td>Average of All Counties</td>
                <td>{{ (data.reduce((total, row) => total+parseFloat(row['% Registered']), 0) / data.length).toFixed(1) }}%</td>
                <td>{{ (data.reduce((total, row) => total+parseFloat(row['% Turnout']), 0) / data.length).toFixed(1) }}%</td>
                <td>{{ (data.reduce((total, row) => total+row['Predictability'], 0) / data.length).toFixed(3) }}</td>
            </tr>
        </tfoot>
    </table>
{% endraw %}

</div>

<script>

// Expects data in the form
// [{col1: 123, col2: 234},{col1: 123, col2: 234},...]
new Vue({
    el: '#sortableTable{{ numSortableTablesOnPage }}',
    data: {
        // JSON data gets injected in here using Liquid command, from our include file
        data: {% include {{include.json}} %},
        sortColumn: null,
        sortDirection: -1,
        enableAnchorLinks: {{ page.list_of_counties | size }},
    },
    computed: {
        headings() {
            return Object.keys(this.data[0]);
        },
    },
    methods: {
        slug(string) {
            return string.toLowerCase().replace(/\W+/g, '-');
        },
        sortBy(columnName) {
            // If we're already sorting by this column, flip the direction
            if (columnName == this.sortColumn)
                this.sortDirection = -this.sortDirection;
            else
                this.sortDirection = -1;
            
            // Sort the array (in place)
            this.data.sort((a, b) => {
                a = a[columnName];
                b = b[columnName];
                if (!isNaN(parseFloat(a))) a = parseFloat(a);
                if (!isNaN(parseFloat(b))) b = parseFloat(b);
                if (a > b)
                    return this.sortDirection;
                else if (a < b)
                    return -this.sortDirection;
                else
                    return 0;
            });

            // Remember which column we're sorting by, so we can reverse it later if needed
            this.sortColumn = columnName;
        }
    },
});


</script>

<style>
    .sortableTable th {
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .sortableTable th:hover {
        background-color: #f0f0f0;
    }
</style>