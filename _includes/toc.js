/**
 * Generate a hyperlinked table-of-contents 
 * at destSelector based on the H1, H2, H3, ...
 * tags found inside sourceSelector
 *
 * To exclude certain items from the list, you
 * could use CSS, or otherwise pass in a filterFunction
 *
 * EXAMPLE:
 * 	tableOfContents('#article', '#toc');
 *	   tableOfContents('article', '#toc', item => 
 *			item.tagName != 'H1' && item.heading != 'ABC');
 */
function tableOfContents(sourceSelector, destSelector, filterFunction, overrideFunction) {
	var destElement = document.querySelector(destSelector);
	if (!destElement)
		return;

	var sourceElement = document.querySelector(sourceSelector);
	if (!sourceElement)
		return;

	var headings = sourceElement.querySelectorAll('h1,h2,h3,h4,h5,h6');
	if (!headings.length)
		return;
	
	// Scour the headings to create an array
	// e.g. { tagName, href, heading, children: [] }
	var structure = [];
	for (var i = 0; i < headings.length; i++) {

		// If heading does not have an ID, we cannot link to it, so skip
		if (!headings[i].id)
			continue;
	
		var item = {
			tagName: headings[i].tagName,
			href: '#' + headings[i].id,
			// If we wanted to strip any extra
			// HTML tags *inside* the heading tag
			// we could do:
			// headings[i].childNodes[0].textContent
			// (assuming the text came first)
			heading: headings[i].textContent,
			headingIndex: i,
			first: i == 0,
			last: i == headings.length - 1,
			children: [],
		};
		
		// If a filterFunction is provided, ensure
		// the item passes the test before including it
		if (filterFunction && !filterFunction(item))
			continue;

		if (overrideFunction)
			item = overrideFunction(item);
	
		// Find the right position/depth within our 
		// structure for our item
		var parent;
		
		// Is this a top-level item?
		if (i == 0 || item.tagName <= headings[0].tagName) {
			parent = structure;
			
		// Non-top-level item
		} else {
			// Search through levels of last item
			// to find where this one belongs
			parent = structure[structure.length - 1].children;
			while (parent.length) {
				// If this is correct level for this heading
				// (Are the children lower than current?)
				if (parent[parent.length-1].tagName >= item.tagName)
				break;
				
				// Else, search the next level
				parent = parent[parent.length-1].children;
			}
		
		}
		
		parent.push(item);	
		
	}
	
	// console.log(structure);
	
	// Convert the array to DOM elements
	var list = document.createElement('ol');
	structure.forEach(function(item) {
		list.appendChild(createListItem(item));
	});
	destElement.appendChild(list);
	
	// Recursive function to create a list
	// item along with all child items
	function createListItem(item) {
		var li = document.createElement('li');
		var a = document.createElement('a');
		li.appendChild(a);
		a.href = item.href;
		a.textContent = item.heading;
		if (item.children.length) {
			var ol = document.createElement('ol');
			li.appendChild(ol);
			item.children.forEach(function(child) {
				ol.appendChild(createListItem(child));
			});
		}
		return li;
	}
}
	
tableOfContents('.post-content', '#toc',
	null, // item => item.tagName != 'H1' && item.heading != 'Second heading'

	// Always force the Footnotes & References item to top level
	item => {
		if (item.href == '#footnotes--references') {
			item.tagName = 'H1';
		}
		return item;
	}
);