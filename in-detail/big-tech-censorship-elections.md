---
title: xxx
published: false
---




From the Epoch Times: <https://www.theepochtimes.com/twitter-suspends-2020-maricopa-county-election-audit-accounts_3921097.html>

<p>When trying to access the <a href="https://twitter.com/ArizonaAudit" target="_blank">Maricopa County Audit</a>, the <a href="https://twitter.com/disclosetv/status/1420078126483312655?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1420078126483312655%7Ctwgr%5E%7Ctwcon%5Es1_&amp;ref_url=https%3A%2F%2Fwww.zerohedge.com%2Fpolitical%2Ftwitter-suspends-2020-election-audit-accounts-multiple-states" target="_blank">Audit War Room</a>, and various “war room” accounts dedicated to <a href="https://twitter.com/auditwisconsin" target="_blank">Wisconsin</a>, <a href="https://twitter.com/auditNevada" target="_blank">Nevada</a>, <a href="https://twitter.com/auditpennsylvania" target="_blank">Pennsylvania</a>, <a href="https://twitter.com/auditmichigan" target="_blank">Michigan</a>, and <a href="https://twitter.com/auditgeorgia" target="_blank">Georgia</a>, Twitter wrote: “Account suspended.” The War Room account had more than 40,000 followers and the Maricopa County Audit account had nearly 100,000 followers.</p>