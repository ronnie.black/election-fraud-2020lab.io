---
title: Shawn Smith Testifies to Colorado Secretary of State
breadcrumb: "[Seth Keshel Election Analysis](/seth-keshel-reports/) › [Colorado](/seth-keshel-reports/colorado/) ›"
meta_desc: Shawn Smith shares grave concerns about election integrity in the state.
---

*During Colorado Public Rulemaking Hearing, August 3, 2021. Shawn Smith is a Retired Colonel of the US Air Force.*

I'm a Colorado citizen, an eligible voter, I represent myself, I'm here to honour the sacrifices made by my brothers and sisters buried under our flag.

I publicly dare any public official to call me a conspiracy theorist when I speak to my concerns with our election and election systems. I'm dismayed and disgusted by the unrelenting partisanship and condescending authoritarian attitude and conduct of the Colorado Secretary of State, aided and abetted by mute acquiescence of the Colorado Attorney General. I have a right to a free and fair election, a right to the transparency to satisfy myself that our elections and our election system is free from and not vulnerable to fraud and corruption. I am *not* satisfied or convinced that our 2020 election was free and fair or that our election systems are secure.

I have good reason to believe that our election systems were not secure and never have been and I have provided expert testimony on that basis to our general assembly and to a multitude of election officials and a disproportionate majority of them have disregarded that testimony. Every election official in the State of Colorado was working for the people, that's me, among other citizens. I don't need to be told by anyone that our election system was "The Gold Standard". I don't believe that, and every time you say it, I doubt you more.

The only reason you're required by US and state law to retain all the election records is for the people to be able to satisfy themselves that our elections were or were not free and fair, so every statement that you make trying to discredit the very idea of independent forensic audits and these bogus unconstitutional rules trying to prohibit forensic audit of our election and election systems, to restrict poll-watchers, to make it easier to fraudulently register or to fraudulently vote are a slap in the face to citizens and a direct challenge to the consent of the governed in the State of Colorado.

Your obliviation about accreditation and chain of custody is ludicrous. *You* aren't accredited. Neither is anyone in the county. Neither is the voting vendor themselves. And you have no chain of custody, the systems were built overseas, unsupervised by any sworn official, and shipped without security or chain of custody. Every Colorado citizen has a right to vote once and have that vote accurately counted and not altered or diluted by fraudulent votes and miscounting. No citizen has a right to convenience in voting, like registering or voting online, or by mail, or to efficiency if that convenience or efficiency comes at the price of any reduction in integrity. Any measure you take, and any effort you make to reduce integrity for efficiency or convenience is unethical and a betrayal of your duty.

You had no hearing for emergency rules because it was an "emergency". *What emergency?* For that matter, what public concern? We've made Colorado open record act requests for the basis of the emergency rules and the Secretary of State and Colorado Attorney General and their staffs have basically said "[let them eat cake](https://en.wikipedia.org/wiki/Let_them_eat_cake){:title="In other words, we leaders will disregard you peasants"}". Who in the hell do you think you are? Your duty is to the Constitution and we are telling you we are dissatisfied. You don't have hundreds of comments, you have *thousands* of comments.

I request, respectfully, as a citizen that the Secretary of State reverse course, restore public trust, and allow -- as we demand -- forensic, independent audit of our election and election systems.

Thank you very much.

----

*Shawn also presented at [Mike Lindell's Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) along with Tina Peters, and has also [demonstrated on video](https://rumble.com/vl7y6g-cyber-symposium-simulation-of-how-your-votes-can-be-flipped.html) how insecure the voting machines are.*

He also appears in [this YouTube video](https://www.youtube.com/watch?v=1HmU3P5pi08), speaking about election issues.

----

### Audio Version

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vi4lv9/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<small>Source: [Rumble video](https://rumble.com/vkqs0j-colorado-secretary-of-state-election-rules-hearing.html), with further sources [here](https://www.qnotables.com/post/colorado-secretary-of-state-moves-to-eliminate-signature-verification) and [here](https://www.sos.state.co.us/pubs/info_center/audioBroadcasts.html).</small>