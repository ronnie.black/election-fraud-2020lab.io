---
title: Dr. Shiva's Open Forum for Maricopa County
meta_desc: 4-hour open forum held by Dr. Shiva to openly dialogue about his findings and unresolved questions from the Maricopa County audit in Arizona. We show full video and our dot-point summary.
last_updated: 15 Oct 2021
---

{:.small.muted}
14 Oct 2021

Dr. Shiva Ayyadurai, MIT PhD, the Inventor of Email [^1], is a world-renowned engineer, scientist, educator, entrepreneur, activist, and author. He is a Fulbright Scholar, Westinghouse Science Honors Awardee, Nominee for the National Medal of Technology and Innovation, who has published in major peer-reviewed journals such as IEEE, Nature Neuroscience, CELL’s Biophysical Journal, to name a few. He advocates that only a Systems Science Approach can provide a comprehensive method to understand connections among the parts of any system to elicit the scientific truth. 

Dr. Shiva was a key auditor in the Maricopa County Forensic Audit. Shiva has not advocated for either Trump or Biden, and in fact did not vote for either Trump or Biden in 2020. [^2]

On Oct 14, 2021, he invited Republicans, Democrats and Maricopa officials to a bipartisan, open dialogue about the unresolved questions and outstanding issues, and to get to the root issues affecting election integrity. Here is the full 4-hour video recording:

----

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bF4nMoX0IkY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

----

Here is a summary of some of the key points raised during the 4 hour open forum:

- Elections are very complex systems now. But they lack transparency.

- Are we measuring the precision in these very complex processes? It seems that this is generally very lacking.

- Transparency is also a huge issue. As one example, the Standard Operating Procedures (SOP) of those running elections should be fully disclosed, but this has yet to happen.

- Shiva is joined by Phil Evans, who has been analyzing elections since 2011. Last year he met Dr Shiva, and after investigating, observed how Dr Shiva's senate-run election was rigged. They've been doing analysis in Michigan, Georgia, and now Maricopa.

- Shiva's team has sought to have cooperation with Maricopa County officials (and those in other states), but those efforts are largely refused.

- Upon reaching out to Runbeck, the company responsible for coordinating many of the mail-in ballots including the envelope scanning, they replied that they were under instruction not to answer any questions, and that inquiries would have to go directly to Maricopa County.

- In Maricopa, 91-92% of all ballots cast came through the early voting ballots (known as EVBs), that's over 1.9 million. Quite a significant number! This increased from 1,257,179 in 2016.

- It's highly important to get the ballot envelope verification step correct. If this is done incorrectly, it disrupts the validity of the ballot count. (That is, if illegitimate ballots make it through the envelope verification process, the final count is going to be inaccurate and illegitimate.)

- Shiva and his team still have the following unanswered questions about Runbeck's process:

  - What is your process?
  - Can images be altered?
  - Who has access to these images?
  - Why did you compress these images so heavily?
  - Why did the compression method result in odd artifacts such as outlining solid objects?

- The only response they've heard thus far about the reason for compression is that "we want to save money". But for an extra couple of thousand dollars, they could have retained full-resolution, uncompressed images of all ballot envelopes, which would have been far more reliable to audit.

- The signature verification is often performed by volunteers. Even left-leaning journlists in The Atlantic and LA Times have often called this process "a witchcraft process" and ripe for error.

Dr. Shiva reviewed his report on the Maricopa County election ballot envelopes, and reviewed his seven major findings:

**Anomaly&nbsp;1.** | Maricopa reported only 587 "bad signatures" out of 1,918,463 (0.031%), which is one out of every 3,268. This seems low. Did they relax the signature rules (27 points of identification) too much?
**Anomaly&nbsp;2.** | 6,545 more ballots (EVBs) than return envelope images
**Anomaly&nbsp;3.** | Signature rejection rates were 73% lower in 2020 compared to 2016 (from 0.116% in 2016 to 0.031% in 2020)
**Anomaly&nbsp;4.** | No mention of duplicates in Maricopa Canvass Report
**Anomaly&nbsp;5.** | Duplicate blank envelopes were verified and approved
**Anomaly&nbsp;6.** | 66 instances where individuals have 2 different voter IDs, and submitted 2 ballots (identified as the same person by the exact same name, address, phone and signature)
**Anomaly&nbsp;7.** | The "VERIFIED & APPROVED" stamp is seen both behind and in front of envelope triangles in the series of images. What causes this anomaly and are processes being correctly followed?


## Tim Canova

Tim Canova joined the forum. He was a candidate for Florida's 23rd congressional district, as a Democrat in the 2016 Democratic Primary. After noticing odd results in the election he requested an investigation, but instead:

- A basic ballot audit was performed in another district (not his)

- After Canova filed a lawsuit to gain a proper investigation, the county clerk destroyed the ballots, *less than 1 year after the election*, and blamed her staff for her signing it

- Despite the obvious illegal destruction of election materials and illegal destruction of evidence for ongoing litigation, the clerk was never charged, and was instead able to resign and retain her generous county pension.

*"This [corruption] is not about Democrat versus Republican --- this is insider versus outsider. We end up with representatives that don't represent us. They represent all kinds of interests, [including] huge corporate interests."*

*"I ran [again] as an independent in 2018, and the results of that election were [also] very suspicious because I was cast the exact same low percentage point in every precinct in the district, and the same exact percentage point among every demographic group: whites, blacks, hispanics, Republicans, Democrat, males, females -- it didn't matter. I got capped at the same percentage. One expert in computational science who you might know: Dr David Bader concluded the chances/odds of that election result was as unlikely as winning the lottery every day for a year."* [^3]

**It's important that people on both sides, the left on the right, feel comfortable with how their vote was counted.**

----

Further points raised:

* Shiva has been doing analysis on Michigan, and now looking at Pima county. His team previously analyzed Massachusetts themselves, after losing his senate-run.

* *"As an MIT PhD who's done many audits, [it's clear to me that] they are not transparent. We're not saying people are cheating --- [although] we do know certain people are cheating in certain elections, but not everywhere --- the lack of transparency is the real fraud. The real fraud in this county is that we have people who don't want to talk about the election processes, and we have really "numbskull" media people who don't have any training in mathematics or systems [writing things about this election]."*

* Kentucky is struggling to get voter rolls. Have they issued a FOIA request? They may need to do that.

* *"It has always been the bottom-up movement that has addressed this. Working people."*

* *"If these election officials ran airport systems or train systems, there'd be airplane crashes nearly every day."*

- Without real insight, we have leaders creating fake problems, and fake solutions.

- It needs to happen from the working class people. Go local, go local, fight local.

- Visit VASHIVA.com for training in political theory.

----

## Liz Harris 

Liz Harris joins the forum at 2:43:30.

*"There errors we found in our voting canvass, the same exacts errors in appearing in the Michigan canvass, and by that I mean the official record of what happened in November 2020."*

*"We didn't just canvass to the door, we actually took a sample set and we made phone calls to verify the information."*

Liz has a doctorate and an MBA in other subjects, yet people continue to attack Liz's methodology. 

Shiva highlights that there are various methodologies you apply to canvassing:

- How you select your sample size
- How you ask your questions 
- Who you target
- How you apply your result to a larger population size

These are important to carefully consider.

----

Steve Larsen, Polk County, Florida, President of Republican Club, leader for Defend Florida in the county, called in. He's canvassing the county's results for accuracy, and stated:

*"I ran for City Commissioner two years ago, and I lost the election. Afterward we asked for the phone records of the City Commissioners and in those text messages we got, it said that one commissioner was asking another commissioner do you have any more mail-in ballots, and the commissioner's response was "Yes, I'll bring some over". I called the secretary / supervisor of elections and he said you've got to call the Secretary of State. So I called them, and they said you've got to call the District Attorney. They just run you around in circles and circles until you give up. I didn't have the money for a lawsuit.*

----

At 3:17:00 a caller mentioned the compression issues --- that ballot envelope images cannot appear with outlined triangles like that through simple compression (it must be some obscure compression technique).

----

Shiva says: *"The reason Maricopa occurred, [was that] Phil and I did a presentation at a public hearing, where they tried to resimulate what would occur. That video went viral and really got people concerned about the improbability of the Donald Trump results against Biden."* Trump later tweeted out "Impossible Result".

----

A caller at 3:37:00, from Pima County, said that they received 3 mail-in ballots to their address. One was addressed to them, the other two to someone else. The valid one had a valid US postage stamp, the other two did not, appearing that they did not go through the proper, legal mail system.

----

Shiva's team are keen to provide audit tools, for grassroots people to use.

Email [vashiva&#64;vashiva.com]({{site.data.globals.mailtoPrefix}}vashiva&#64;vashiva.com) or [drshiva&#64;vashiva.com]({{site.data.globals.mailtoPrefix}}drshiva&#64;vashiva.com) for more information.

Dr Shiva can also be contacted via phone on 617-631-6874.

----

### Footnotes & References

[^1]: His claim of being the inventor of email has been disputed, and his reputation smeared on places like like Wikipedia, but we believe his very extensive evidence for the claim, including attestations from other computer experts validates its truth. See <https://www.inventorofemail.com/thefacts/>

[^2]: Dr. Shiva never voted prior to 2016 because he did no believe any candidate was going to produce the necessary change. He voted for Trump in 2016 because he saw him speaking against the establishment, and yet grew critical of Trump due to lack of action against some ingrained establishment issues, and did not vote for him in 2020. Despite the criticism, Trump has still acknowledged the beneficial work of Dr. Shiva. See a summary at <https://wendyrogers.org/wp-content/uploads/2021/10/Dr-Shiva-Replies-to-Maricopa-County-Officials.pdf> or in the video.

[^3]: This quote is from the video, however Tim Canova also writes about it in this article: <https://www.sun-sentinel.com/opinion/commentary/fl-op-com-count-votes-and-count-by-hand-20190729-xysvd46l4faj3lxfbno6ry4p3i-story.html>, Jul 29, 2019 

