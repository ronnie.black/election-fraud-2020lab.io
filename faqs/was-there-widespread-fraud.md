---
title: Was there really widespread election fraud?
TODOs:
  - Link to politicians who said "there was no widespread fraud"
  - Link to Patrick Byrne's research
last_updated: 11 Jul 2021
---

Several politicians have made strong claims that "there was no evidence of widespread fraud during the 2020 election".

We believe the material and evidence presented on this website indicates that there was likely to have been intentional fraud across several states to the degree that it could be "widespread".

However, as Patrick Byrne's research pointed out [^1], the fraud does not have to be "widespread" to greatly affect the election. Instigators of the fraud only needed to focus on a few key swing states, and the notable counties within those states. If those could be flipped, it would be enough to swing the entire Presidential election result.

The six states that were considered the "battleground states" or "swing states" were:

* [Arizona]({% link _states/arizona.md %})
* [Georgia]({% link _states/georgia.md %})
* [Michigan]({% link _states/michigan.md %})
* [Nevada]({% link _states/nevada.md %})
* [Pennsylvania]({% link _states/pennsylvania.md %})
* [Wisconsin]({% link _states/wisconsin.md %})

These were indeed the six states that had the most irregularities. 

Whether or not the fraud was "widespread" is not the key issue. (One could say that *any* fraud or more than a few ballots should be thoroughly investigated.) But the issue of national importance is whether the fraud was extensive enough to alter the outcome of who was elected to the Presidency and the positions of Congress. Since only a few key counties became the deciding factors in these results, all credible claims deserve to be thoroughly investigated.

### Footnotes & References

[^1]: Patrick Byrne, traditionally a Libertarian, developed a team of computer specialists to investigate election integrity issues including the notorious one-sided "spikes" found in several states and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. See "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" (Nov 24, 2020) and his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".