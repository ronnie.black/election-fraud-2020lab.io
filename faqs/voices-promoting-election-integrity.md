---
title: Where can I find organisations promoting election integrity?
TODOs:
last_updated: 28 Jul 2021
comment_intro: Know of any other organisations supporting election integrity? Let us know in the comments below.
---

We're aware of the following organizations that promote election integrity through audits and measures to prevent fraud in future elections:

* [Defending The Republic](https://defendingtherepublic.org/) -- from attorney and former federal prosecutor Sidney Powell

* [The America Project](https://theamericaproject.com/) -- raising funds for the Maricopa Audit in Arizona and others elsewhere, from Lt. General Michael T. Flynn and Patrick Byrne. See also [FundTheAudit.com](https://www.fundtheaudit.com/).

* [VoicesAndVotes.org](https://voicesandvotes.org/) -- from Christina Bobb of One America News


