---
title: "Down Ballot Audit Force: Worked Examples"
breadcrumb: "[Follow the Data](#colophon) › [Down Ballot Audit Force](/audit-force/) ›"
meta_desc: Help us collect important data on the 2020 election. Here we show some worked examples.
last_updated: 05 Dec 2021
wide_layout: true
---

{% include toc %}

# 📝 Worked Examples

We **strongly recommend** you work through these examples at your own pace, in order to acquire the skills and confidence you need to do this on your own, for your county.

**REMINDER:** This is a real working example. We will find, save, collect and analyze **real** election results.

## 📗 Tarrant County, TX

**HINT:** Click on an image to enlarge it.

### Background

- Total Votes in 2020: **834,697**
- Always voted Republican until 2020
- Voted Democrat for the first time in 2020

### Finding The Results

Open your web browser and search for `tarrant county texas 2020 election results`.

Or, search for `tarrant county texas official website`. Once you find the official website you should be able to find the "election results" page.

You may need to try with different search engines, such as DuckDuckGo, Yahoo or Bing.

You should eventually come across this page: <https://access.tarrantcounty.com/en/elections.html>

Yes, you will need to carefully read the page and perhaps try different links until you find the right one.

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/e708707584cfe07f1375ab6081820d91/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/e708707584cfe07f1375ab6081820d91/image.png)

Follow the link to this page:  
<https://access.tarrantcounty.com/en/elections/election-archives.html?linklocation=Voter%20Resources&linkname=Election%20Archives?linklocation=Iwantto&linkname=Elections%20Results%20Archives>

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/70dbff88c81fc8e96d8fe7ead583c710/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/70dbff88c81fc8e96d8fe7ead583c710/image.png)

Click on the "2020" link: <https://access.tarrantcounty.com/en/elections/election-archives/2020-archives.html>

Several elections will be listed. Find the one held in **November** or that mentions "**General Elections**".

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/151d0a6ed5fc26294afa6adbe286600c/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/151d0a6ed5fc26294afa6adbe286600c/image.png)

They offer several different report formats

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/afc45a90177e89c22fe0b596c6d0d2c2/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/afc45a90177e89c22fe0b596c6d0d2c2/image.png)

Click on the links to see which one is easiest to work with. The information on both pages should be exactly the same.

If there is a page that displays a pdf file, then download it and store it in a safe place.

If you can only find a HTML page, then save it or take screenshots of it.

One of the links will open up a pdf report, and will look like this:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/2eab0eee2d0c6251d8535ec3c66d3330/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/2eab0eee2d0c6251d8535ec3c66d3330/image.png)

The screenshot only shows one page with two different races. The Presidential and Senator races. Other pages have the results for other races.

### Saving The Results

- Download the pdf file to your computer
- Save the file to a safe location
- Rename the file and add the election date to it

Once you have saved all the pdf files for the elections we are interested in, your folder should look like this:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/7cf7d0b13d5d5b21c567711536def497/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/7cf7d0b13d5d5b21c567711536def497/image.png)

### Collecting The Results

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/2c15ab2a7dd48520c22a3193b50a2f5d/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/2c15ab2a7dd48520c22a3193b50a2f5d/image.png)

- We only want to record races that have similar total votes to the Presidential race (within 20%)
- You will notice the total votes for each race vary. That is normal.

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/fe3a515307d3544970ecc1a1c910db5f/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/fe3a515307d3544970ecc1a1c910db5f/image.png)

As you scroll through the report, record your numbers in a spreadsheet or a piece of paper.

Once you get to the end of the report, repeat the process for the year 2016, 2012, 2008, 2004 and 2000.

Once you are finished, your spreadsheet should look like this:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/7bbb12c67e141409f396a12a0c47c987/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/7bbb12c67e141409f396a12a0c47c987/image.png)

### Analyzing The Results

Calculate the percentage votes for each race:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/e1ddce96b0f0e2f5c0ffcf82846a6fde/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/e1ddce96b0f0e2f5c0ffcf82846a6fde/image.png)

Calculate the average gap across all races, **but exclude the presidential race**:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/416cf6ba7b19d6c54d04c92d0ffb8c5f/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/416cf6ba7b19d6c54d04c92d0ffb8c5f/image.png)

Compare the average values with the Presidential value:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/f84eeb2cb3f4717734f6ed7f851c4bbe/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/f84eeb2cb3f4717734f6ed7f851c4bbe/image.png)

Do you see any discrepancies?

When you do the analysis and look at ALL the elections (from 2000 to 2020) you will see that the average percentage across all races is always very similar compared to the Presidential value... except in 2016 and 2020...

You will also notice that the average percentage in 2012 and 2016 are very similar (16.4% and 15.6% respectively). How did it drop to 6% in 2020 in just one election cycle? (That is a drop of 10% in one election cycle!?!)

Not to mention the Republican party loosing majority by 0.2%, for the first time in a long time, in 2020!

### Sharing the Results

Once you have finished your analysis, double check your work and share the information with family and friends.

If the results raise enough doubts in **YOUR** mind, demand a full forensic audit of **YOUR** county.

Unite with other people in your county in demanding full transparency.

----

## 📘 Williamson County, TX

You should now be in a position to do Williamson County, TX on your own.

We recommend you try doing it entirely on your own first and only look at the guide if you get stuck. Once you have finished you can compare your results with ours.

**NOTE:** The election results for each election use slightly different formats.

### Background

- Total Votes in 2020: **290,168**
- Always voted Republican until 2020
- Voted Democrat for the first time in 2020

### Finding The Results

Open your web browser and search for `williamson county texas 2020 election results`.

Or, search for `williamson county texas official website`. Once you find the official website you should be able to find the "election results" page.

You may need to try with different search engines, such as DuckDuckGo, Yahoo or Bing.

You should eventually come across this page:  
<https://www.wilco.org/Departments/Elections/Result-Archive>

It will list all the results for each election:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/87dc324000258aa5bcb68f0133d72f96/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/87dc324000258aa5bcb68f0133d72f96/image.png)

Look for the "General Election" results that are held in November.

### Saving The Results

Williamson County does not pdf files for their election results.

Some election results are only available as HTML pages, others as text files.

Save the HTML pages (and text files) using your browser to a safe location.

Once you are finished it should look like this:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/ef4927b88a8fb78aaf0a6ca6698d69a8/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/ef4927b88a8fb78aaf0a6ca6698d69a8/image.png)


### Collecting The Results

Simply copy/paste (or type) the numbers into your spreadsheet or transcribe them onto your piece of paper.

**REMEMBER:** To check the total votes for each race before writing them down. We are only interested in races where the total votes is within 20% of the total votes in the Presidential race.

#### 2020

Link: <https://apps.wilco.org/elections/results/default.aspx?e=346930>

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/3b7e0cffe9d1f8e8bd92cd1ef415ceae/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/3b7e0cffe9d1f8e8bd92cd1ef415ceae/image.png)

Keep scrolling down the page. You should find about 16 valid races.

#### 2016

Link: <https://apps.wilco.org/elections/results/default.aspx?e=432521>

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/b081a82d23533f5a685b88a2bfb93f20/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/b081a82d23533f5a685b88a2bfb93f20/image.png)

#### 2012

Link: <https://www.wilco.org/Portals/0/Departments/Elections/Results/2012/Nov_6th_JGSE_Summary.txt>

We now have raw text files.

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/48f71df54741bd9a5c8a48f071f5697f/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/48f71df54741bd9a5c8a48f071f5697f/image.png)

#### 2008

Link: <https://www.wilco.org/Portals/0/Departments/Elections/Results/2008/November%204th%20Summary.txt>

This is a different format to 2012.

**For these election results we need to calculate the total votes for each race ourselves.**

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/3a1c046b2440f11abaf1c49fad3308bb/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/3a1c046b2440f11abaf1c49fad3308bb/image.png)

#### 2004

Link: <https://www.wilco.org/Portals/0/Departments/Elections/Results/2004/Nov%202nd%20General%20Election.txt>

Again, another text file with a different format to 2008 and 2012.

**For these election results we need to calculate the total votes for each race ourselves.**

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/3723679c805d4e6420fce81bdd926fd7/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/3723679c805d4e6420fce81bdd926fd7/image.png)

#### 2000

Link: <https://www.wilco.org/Portals/0/Departments/Elections/Results/2000/November%202nd%20Presidential%20Election.txt?ver=2017-03-08-143136-480>

We couldn't make sense of the data so we just ignored the 2000 election results. We have enough already to see patterns emerge.

### Analyzing The Results

Here are the final results:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/844b0b27bb3f1d2c3d91af972b64bc5c/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/844b0b27bb3f1d2c3d91af972b64bc5c/image.png)

- What is the deal with the Sheriff election in 2020? Can any locals comment on that?
- When you do the analysis and look at ALL the elections (from 2004 to 2020) you will see that the average percentage across all races is always very similar compared to the Presidential value... except in 2016 and 2020...
- You will also notice that the average percentage in 2012 and 2016 are similar (18.0% and 21.3% respectively). How did it drop to 5% in 2020 in just one election cycle? (That is a drop of 13% in one election cycle!?!)
- Not to mention the Republican party loosing majority by 1.4%, for the first time in a long time, in 2020!!!
- Even in 2008, when Obama won the general election the Republican candidate still won Williamson county with a huge margin of 13%...

When George Bush won his second term in 2004, he had a margin of 31.3% over the Democrats. The margin across all races was similar:

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/468ea69be9a20a873fda7da117866908/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/468ea69be9a20a873fda7da117866908/image.png)

Do the patterns and trends in the 2020 election look normal to you?

If they don't, demand answers! If not you, then who?

### Sharing the Results

Once you have finished your analysis, double check your work and share the information with family and friends.

If the results raise enough doubts in **YOUR** mind, demand a full forensic audit of **YOUR** county.

Unite with other people in your county in demanding full transparency.

## Worked Examples Summary

The two counties that were analyzed, Tarrant and Williamson county, are two out of 32 counties that voted Democrat for the first time in 2020, in a long time.

None of the counties (shown in the table below) voted for Obama in 2008!

If you are curious you can do the analysis for yourself. We believe the results will be startling.

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/08a779793ae8ff282324d6c3146c2a6f/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/08a779793ae8ff282324d6c3146c2a6f/image.png)

Oh, what a coincidence Maricopa county, AZ, is in the list as well...

# Summary

The process of doing a "Down Ballot" analysis for a county is not complicated. It only requires a bit of time. And the results might surprise you!

We can not guarantee that the results of your county will be as decisive as the worked examples, but we are certain there are many counties that are just as compelling if not more so.

You should now have the knowledge you need to analyze your own county.

Keep this guide handy for reference purposes, and if you really get stuck, don't hesitate to ask for help on the Telegram channel.

Once you completed the analysis and double checked your results with others, make sure to share it with relative and friends. And post your results on our Telegram channel.

# Feedback and Suggestions

We welcome all feedback and suggestions to help improve this guide.

Perhaps you have found some search terms that are more effective at finding an official county website?

Some sections might be confusing and need re-writing?

Please let us know.

👓 If you like numbers, you might also be interested in: [Identify Electoral Fraud Using Trend Analysis](/trend-analysis/).