---
title: Validating Voter Roll Data
parent_page: /data/voter-rolls/
last_updated: 10 Nov 2021
draft_status: true
# wide_layout: true
---

* Checking the snapshot date is correct
* Checking for common issues
* Differentiating between poor admin and intentional manipulation

----

{% include article_series from="/data/voter-rolls/" telegram=false %}
