---
title: Finding Anomalies & Fraud
parent_page: /data/voter-rolls/
last_updated: 10 Nov 2021
draft_status: true
# wide_layout: true
---

* Ineligible voters 
  - Deceased voters 
  - Non citizens
  - Incarcerated
  - Voters with lapsed records
  - Underage voters

* Registration date patterns
  - By day of week
  - Fluctuations over time
  - Or LACK of fluctuations over time 

----

{% include article_series from="/data/voter-rolls/" telegram=false %}
