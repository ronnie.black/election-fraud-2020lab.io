---
title: List of Unlikely z-Score T/DR Values Across All Counties in 2020 Election
meta_desc: 
parent_page: /trend-analysis/unlikely-z-scores/
last_updated: 15 Nov 2021
wide_layout: false
---

From our article on *[Unlikely z-Score values](/trend-analysis/unlikely-z-scores/)*, here is the full list of counties (across the whole country) where, in 2020, the z-Score value for the Democrat Votes divided by Democrats Registrations (zTDR) is greater than 2.5.

<div class="tip emoji" markdown="1">
💡 **TIP:**  
All these counties are highly suspicious and would warrant further investigations. 

Values over 2.5 | Rare (typically only 1 in 100) | 🧐
Values over 3 | *Extremely* unlikely | 😲
Values over 4 | *"Super extremely"* unlikely | 😱

In other words, values over 4 should be ***super extremely rare***, yet there are **116** counties across 14 states with a zTDR greater than 4!

- Louisiana has 12
- Pennsylvania has 13
- Florida has 15
- North Carolina has 18
- Oklahoma has 34 😳

</div>

{% include large_image url="/images/trend-analysis/trends/zTDR_1.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_2.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_3.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_4.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_5.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_6.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_7.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_8.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_9.png" maxWidth="1400" %}

<!--
These two seem to be missing from the repo. Guess they were accidental extras?

{% include large_image url="/images/trend-analysis/trends/zTDR_10.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zTDR_11.png" maxWidth="1400" %}
-->


----

{% include article_series from="/trend-analysis/" %}