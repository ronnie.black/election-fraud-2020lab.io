---
title: Election Data Inquiries
meta_desc: Need assistance?
parent_page: /trend-analysis/
last_updated: 10 Oct 2021
comment_intro: Do you have any feedback for how we can make this website more helpful and practical? Or did you find an error that needs correcting? Let us know in the comments below.
---

We aim to assist genuine and honest inquiries into what happened during the 2020 General Election.

After spending over 8 months collating information and analyzing all the election data we can find, there is no doubt in our minds the election results were compromised.

We are bipartisan --- not affiliated with the Democrat nor the Republican party, nor any party for that matter. We simply stand for *Truth, Justice, Honesty and Transparency*.

The analysis and reports on this website are done by unpaid volunteers in their free time. Before making any inquiries please familiarize yourselves with the following articles first. They will help establish some common understanding before initiating a conversation:

- [The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/)
- [The Battle of the Largest Counties](/in-detail/battle-for-largest-counties/)
- [The Curious Case of the 2020 "Voting Rate" Blowouts](/trend-analysis/historical-voting-rate-trends/)


{% include toc %}


## Corrections

We aim to provide truthful, accurate and honest information to the public. If you have found an error in our analysis we will gladly correct it. You can notify us via the comments section at the bottom of most pages (preferred), [Gitlab forum]({{ site.data.globals.gitlabIssuesLink }}), [Twitter]({{ site.data.globals.tweetUsLink }}), or [Telegram]({{ site.data.globals.telegramLink }}).

Also read our [statement of humility](/pray/#statement-of-humility).


## For Grass Root Teams

We can:

- Provide an initial analysis for your state or county to help you garner support to build a team of volunteers.

- Assist by helping you focus your efforts on the best counties to canvass first, in order to achieve the best outcome in the smallest amount of time.

- Provide assistance in compiling the canvassing results into reports and double checking the results.

- Publish interim (and final) results on our website for the whole country and the world to see.

**Establishing fraud, even in smaller counties, vitiates the results for the whole state. When the numbers are wrong in smaller counties, it erodes trust in the legitimacy of counts, processes and equipment used in the larger counties also.**

In order to establish your level of commitment we would kindly ask that you provide us with a ["Down Ballot" analysis](/audit-force/) of the counties you are interested in canvassing, when contacting us.

Then reach out via [Gitlab]({{ site.data.globals.gitlabIssuesLink }}) (preferred), [Email {{ site.data.globals.electionDataEmailEncoded }}]({{ site.data.globals.mailtoPrefix }}{{ site.data.globals.electionDataEmailEncoded }}), [Telegram]({{ site.data.globals.telegramLink }}), or [Twitter]({{ site.data.globals.tweetUsLink }}).


## For County and State Representatives

We can provide you with the same information we outlined in the "Grass Root Teams" section, above.

Furthermore, if you are able to provide us with historical voter roll records, we will do an in depth analysis and provide you with a report at no cost. (Subject to our availability. Please discuss this with us first.)

We can look at:

- Historical voting trends by county and affiliation
- Analyse the registration and voting ratios
- Surface abnormal voting patterns and suspicious registrants, between elections and between counties

Reach out via [Gitlab]({{ site.data.globals.gitlabIssuesLink }}) (preferred), [Email {{ site.data.globals.electionDataEmailEncoded }}]({{ site.data.globals.mailtoPrefix }}{{ site.data.globals.electionDataEmailEncoded }}), [Telegram]({{ site.data.globals.telegramLink }}), or [Twitter]({{ site.data.globals.twitterLink }}).


## For Journalists

We are happy to answer queries from honest journalists who are willing to report our findings honestly and truthfully.

The data is **saturated** with evidence of fraud and wrongdoing.

The articles on this site --- especially the [Trend Analysis](/trend-analysis/) section --- explain how to analyze the 2020 election data in great detail. If you need further clarification in covering this "gold mine" of information, we would be glad to assist you in your efforts.

In order to establish goodwill, we would like to see some evidence of your reporting on one or more of the following issues:

- Democrat officials questioning the results of the 2016 election, and raising concerns about election machine security

- Maricopa County forensic audit

- State legislature hearings where statistical and eyewitness evidence was presented

- Destruction of election materials, logs, and other evidence

- Other statistical anomalies from the election



<!--
{% comment %}

- There is plenty of (video) evidence of elected Democrat officials questioning the integrity of the voting process after the 2016 election. **Why are they deathly silent now?**
    - Since it is obvious (now) that they (had/) have full control of the voting process; If there was any doubt of voting irregularities in the 2016 election, they well and truly had the means to get to the bottom of it. **But they chose not to... Why?**
    - Instead, they chose to fabricate lies and pursue a hoax impeachment. (What does that reveal about their character?)
- As Steve Bannon mentioned during the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/). If the election was clean they would have no reservations conducting full forensic audits. (**If they are innocent, there would be no objections. They would only be too happy to oblige and revel in their victory**). None of the arguments for not conducting full forensic audits make any sense. We know that, and we will not relent.
- The government is drunken with power. All the gerrymandering makes this abundantly obvious. They have forgotten that all the power is vested in "We the people". And "We" demand full forensic audits (includes canvassing) of suspicious counties; that all wrong doing be held accountable, that justice and truth be restored and the rightful President be re-instated.

This is the Steve Bannon quote we referred to:

{:.small}
> They [Democrats] understand they can not win elections of the American people when you go to paper ballots. So they are going to weaponize everything around it, and if you think you've seen their toughest moves, if you think you have seen the hammer drop, we haven't seen anything yet...
> 
> "They have sawn the wind and they are going to reap the whirlwind because the American people are going to demand, full forensic audits of 3 November 2020 in every state.
> 
> "That's what they feared. **If they had the receipts it would be totally open. They would rub your nose in it everyday. They would show you every ballot. There wouldn't have to be any subpoenas. They would turn the routers over. They'd say "You've not just lost, you got skunked. You lost Arizona, you lost Georgia, you lost these states... you lost, you lost, you lost."**
> 
> "**They can't prove it**, they don't have the receipts, so they will use every aspect of lawfare, of the media, of the criminal justice system. Of everything they possibly can... To do what? **To thwart the will of the American people...**
> 
> "...The future is **now** and the future is about [fixing] 3 November."
>
> <small>--- Steve Bannon</small>

{% endcomment %}
-->

If you haven't published any articles yet on any of the points mentioned above, we would kindly ask you to provide the answers to the tasks discussed in *[The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/)* and a summary of your findings, with your queries. This demonstrates your level of commitment and understanding, and helps us prioritize inquiries.

Reach out via [Gitlab]({{ site.data.globals.gitlabIssuesLink }}) (preferred), [Email {{ site.data.globals.electionDataEmailEncoded }}]({{ site.data.globals.mailtoPrefix }}{{ site.data.globals.electionDataEmailEncoded }}), [Telegram]({{ site.data.globals.telegramLink }}), or [Twitter]({{ site.data.globals.twitterLink }}).

