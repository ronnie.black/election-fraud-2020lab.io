---
title: The Counties Where Votes and Party Registrations Don't Align
meta_desc: We build on some of the previous techniques to scan 3,111 American counties, identifying those whose shift in vote totals moves unexpectedly against the shift in party registrations.
parent_page: /trend-analysis/
last_updated: 08 Nov 2021
comment_intro: Did you find this article helpful? Did you dig in and review the data for your county or state? What did you find? Let us know in the comments below.
---

<!-- One idea for this article might be to show a scatter chart showing the correlation between DRI/RRI and DVI/RVI - how votes normally follow the trend of registrations. Perhaps we could highlight on the chart the counties that buck this trend. And it would be interesting to compare the chart over several years to observe how the anomalies developed. -->

<div class="tip emoji" markdown="1">
📃 This article is Part 6 in a series on [Identifying Electoral Fraud Using Trend Analysis](/trend-analysis/). 

We recommend reading these preceding articles in order to understand the context and terms used:

* [Part 4: When Winning Margins Go “Off the Charts”](/trend-analysis/3-historical-county-trends/)
* [Part 5: How to Predict Election Results Using Registration Data](/trend-analysis/historical-county-analysis-registration-data/)
</div>

In this article we will analyse every county for which we have registration data, to see if the voting and registration trends align.

We'll group counties into categories based on whether they trended "normally" or "abnormally", and incorporate this into the "winning ranges" table we developed in [Part 4](/trend-analysis/3-historical-county-trends/).

{% include toc %}


## Finding Counties With Unusual Trends

In [Part 5](/trend-analysis/historical-county-analysis-registration-data/) we gathered the party registration numbers for most counties in the US, as well as the votes cast for each party over the last 5 elections. We can now use this data to find out if the voting trends align with the registration trends.

Depending on the results we can group the counties into 4 different categories (or buckets):

Y-D <span class="small" style="white-space: nowrap">(Yes-Democrat)</span> | Voter registration increase favors the Democrat party, and so does the vote increase. "Yes" means the trends align.
N-D <span class="small" style="white-space: nowrap">(No-Democrat)</span> | Voter registration increase favors the Democrat party, but the vote increase does not. "No" means the trends do not align.
Y-R <span class="small" style="white-space: nowrap">(Yes-Republican)</span> | Voter registration increase favors the Republican party, and so does the vote increase. "Yes" means the trends align.
N-R <span class="small" style="white-space: nowrap">(No-Republican)</span> | Voter registration increase favor the Republican party, but the vote increase does not. "No" means the trends do not align.

We'll call this categorization the "Registration and Voting Trend", or "RVT" for short.

{% include expandable_box
    title="Show how to calculate the category in Excel"
    content="

If you're using spreadsheet software like Excel, you can use a formula to determine which category a county falls into.

You'll first need to have separate columns for each of these values:

* DRI --- Increase in Democrat registrations over the prior election
* RRI --- Increase in Republican registrations over the prior election
* DVI --- Increase in Democrat votes over the prior election
* RVI --- Increase in Republican votes over the prior election

And then in another column, use a formula like this, replacing all three-letter codes with the actual cell references, like A1, B2, C3, etc.:

```
=IF(DRI > RRI,
    IF(DVI > RVI, \"Y - D\", \"N - D\"),
    IF(DVI > RVI, \"N - R\", \"Y - R\")    
)
```
" %}

#### Categorization Example

Let's look at the data from Pennsylvania as an example. Here are the most populated counties in Pennsylvania, with their corresponding RVT categories for 2020 and 2016, shown in the last two columns:

{% include large_image url="/images/trend-analysis/trends/PennsylvaniaTrend.png" maxWidth="800" %}

To show an example of how we arrived at this categorization, take a look at the historical results for Philadelphia and Allegheny counties:

{% include large_image url="/images/trend-analysis/trends/PennsylvaniaHist.png" maxWidth="800" %}

It shows that in Philadelphia in 2020, the increase in Republican registrations (RRI) is greater than the increase in Democrat registrations (DRI), and the increase in Republican votes (RVI) is greater than the increase in Democrat votes (DVI). This puts it in the RVT category of "Y-R". (This might come as surprise to some people. Philadelphia has a lot of issues, but is nowhere near as bad as other PA counties, such as Allegheny.)

For Allegheny County, the RRI in 2020 is greater than the DRI, yet the RVI is lower than the DVI, so the RVT is "N-R". In other words the registration increase favored the Republican party, but the vote increase favored the Democrat party. As the numbers show, the difference is ***significant*** and highly suspicious (especially since the DRI was negative and yet the DVI was a very large positive number, nearly ***15 times larger*** than the 2008 DVI value achieved by Obama!).

{% include large_image url="/images/trend-analysis/trends/PennsylvaniaTop2.png" maxWidth="800" %}

<div class="tip emoji" markdown="1">
💡 **TIP:**  
For any given county:

- If the DRI is negative, yet the DVI is positive, we add a star "*" next to the category. 

- If The DRI is negative, and the RRI is positive, yet the DVI is **greater** than the RVI, we add two stars "**" next to the category.

The stars are meant to emphasize that a county is **highly** suspicious and requires further investigations. 

The more stars a county has, the more suspicious it is.

Keep an eye out for the trend bucking counties with a "N-R" category, with possibly one or two stars. (i.e. having a "N-R" category is already very suspicious. Having a star just makes it much worse.)
</div>

**Observations:**

Many "N-R" counties also have uncharacteristically low Loc.20 values. This is important, because these are two independent analyses that have corroborating results. In other words, it reinforces the validity of the "Winning-ranges" analysis and the importance of the Loc.20 value.

## Which States Have The Most Abnormal Trends?

Now let's categorize all counties across the US where we have access to party registration information. We can now filter the categories to show just the counties that bucked the trend --- that is, counties with a RVT of "N-R" or "N-D".

[View all trend-bucking counties, by state](/trend-analysis/rvt-trend-anomalies/){:.button}

You might like to refer to the link above as we summarize the findings below.

There are several states that look very suspicious. 

They are easy to identify because they have many counties with a "Y-R" category in 2016 but a "N-R" category in 2020. In other words, these are the states that had a shift towards higher numbers of Republican registrations *and* votes in 2016, but in 2020, despite a larger increase in Republican registrations, somehow ended up with **more** Democrat votes (!?). 

These states are:

- Colorado (With lots of double stars!)
- Connecticut
- Delaware
- Florida
- Kentucky (With lots of double stars!)
- Louisiana (With lots of double stars!)
- Maine
- Maryland
- Massachusetts
- Nebraska
- Nevada
- New Hampshire
- New Mexico (With lots of double stars!)
- North Carolina (With lots of double stars!)
- Oklahoma (With lots of double stars!)
- Pennsylvania (With lots of double stars!)
- West Virginia (With lots of double stars!)
- Wyoming

Finally there are states that have a lot of "N-R" and "N-D" categories, which would suggest either one of two things (or both!):

1. The voter rolls are completely inaccurate and need to be purged
2. Either the registrations or the votes, or both, were tampered with during the 2020 election

These states are:

- California
- Idaho
- Iowa
- Massachusetts
- New Jersey
- New York
- North Carolina
- Oklahoma
- Oregon
- Rhode Island
- Utah

<div class="info emoji" markdown="1">
🤯 In 2020, out of 3,111 counties, 32 have a "N-D" category, and 199 have a "N-R" category.

<!-- (For comparison, in 2016, 153 counties had a"N-D" category, and 81 had a "N-R" category). -->

This suggests there are over 6 times more counties which did not receive as many Republican votes as would be expected from the registration data, and they all happen to be in the ***most populated*** counties for their respective states. [Coincidence?](/in-detail/battle-for-largest-counties/) (Why are the smaller counties much better behaved than the largest ones?)

This can be clearly seen in the first image above for Pennsylvania. All the smaller counties have a "Y-R" value in 2016 and 2020, however the larger counties have more "N-R" values in 2020.
</div>

## Conclusion

With so many counties bucking trends and setting new turnout records, combined with the reports of poor security and election mishandling, the results do look suspect. Statistics such as these can help identify which counties need a thorough inspection. We believe contacting voters directly (canvassing) combined with forensic audits (where necessary) is crucial to conclusively determining the validity of the election.

----

As you may have noticed, many states were missing from the RVT trend analysis. The reason is that many states don't keep (or don't share) registration records by party. When this is the case we can fall back on the Democrat Vote Increase (DVI) trends.

Our next article, *[Investigating the Large Democrat Vote Increases](/trend-analysis/large-democrat-vote-increases/)* compares these Democrat votes with previous elections, revealing some very large increases in unlikely places.

----

{% include article_series from="/trend-analysis/" %}

