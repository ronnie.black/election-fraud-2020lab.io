---
title: Bellwether Winning Ranges
meta_desc: Winning ranges for the best bellwether counties
parent_page: /in-detail/bellwether-states-counties-2020/
last_updated: 15 Sep 2021
comment_intro: Have any thoughts or feedback on the data presented above? Let us know in the comments below.
---

From [The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/) article. We identified 10 modern bellwether counties that consistently voted over 50% for the winning candidate since the year 2000.

They are shown in the table below:

![](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/uploads/7ab2fb2f6c4532376bbfc639bbb69011/image.png)


We produced the winning ranges for each of the bellwether counties, which you can see below. However before we do, we would like to point out:

<div class="important emoji" markdown="1">
🤯 Out of the 10 bellwether counties, 9 of them saw a **decrease** in percentage vote for Obama's second term (in 2012).

In 2020, all 10 bellwether counties **increased** their percentage vote for Trump from 2016.

Average percentage increase from 2008 to 2012 (Obama): **-2.16%**

Average percentage increase from 2016 to 2020 (Trump): **4.35%**
</div>

## Warren County, IL

{% include large_image url="/images/trend-analysis/winning-ranges/WarrenCounty_IL.png" maxWidth="1300" %}

## Bremer County, IA

{% include large_image url="/images/trend-analysis/winning-ranges/Bremer_IA.png" maxWidth="1300" %}

## Union County, IA

{% include large_image url="/images/trend-analysis/winning-ranges/Union_IA.png" maxWidth="1300" %}

## Traverse County, MN

{% include large_image url="/images/trend-analysis/winning-ranges/Traverse_MN.png" maxWidth="1300" %}

## Ransom County, ND

{% include large_image url="/images/trend-analysis/winning-ranges/Ransom_ND.png" maxWidth="1300" %}

## Sargent County, ND

{% include large_image url="/images/trend-analysis/winning-ranges/Sargent_ND.png" maxWidth="1300" %}

## Wood County, OH

{% include large_image url="/images/trend-analysis/winning-ranges/Wood_OH.png" maxWidth="1300" %}

## Marshall County, SD

{% include large_image url="/images/trend-analysis/winning-ranges/Marshall_SD.png" maxWidth="1300" %}

## Essex County, VT

{% include large_image url="/images/trend-analysis/winning-ranges/Essex_VT.png" maxWidth="1300" %}

## Forest County, WI

{% include large_image url="/images/trend-analysis/winning-ranges/Forest_WI.png" maxWidth="1300" %}

{:.info.emoji}
💡 Look for the number "20". What do you notice about where the 2020 results are positioned within the previous winning ranges?

