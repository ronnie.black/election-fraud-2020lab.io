---
title: Unlikely Z-Score Values
meta_desc: We compare key parameters in the 2020 results against the previous five elections using the z-score, and find hundreds of counties breaking statistical norms.
parent_page: /trend-analysis/
last_updated: 16 Nov 2021
comment_intro: Did you find this article helpful? Did you dig in and review the data for your county or state? What did you find? Let us know in the comments below.
---

<div class="tip emoji" markdown="1">
📃 This article is Part 8 in a series on [Identifying Electoral Fraud Using Trend Analysis](/trend-analysis/). 

We recommend reading these preceding articles in order to understand the context and terms used:

- [Part 3: The Curious Case of the 2020 “Voting Rate” Blowouts](/trend-analysis/historical-voting-rate-trends/)
- [Part 4: When Winning Margins Go “Off the Charts”](/trend-analysis/3-historical-county-trends/) 
- [Part 5: How to Predict Election Results Using Registration Data](/trend-analysis/historical-county-analysis-registration-data/)
- [Part 6: The Counties Where Votes and Party Registrations Don't Align](/trend-analysis/further-registration-trends/)
- [Part 7: Investigating the Large Democrat Vote Increases](/trend-analysis/large-democrat-vote-increases/)
</div>

We discussed the z-score concept in [Part 3: The Curious Case of the 2020 “Voting Rate” Blowouts](/trend-analysis/historical-voting-rate-trends/).

The z-score gives us an indication of how far a value is from the mean, when measured in standard deviation units.

It allows us to normalize results across multiple datasets in order to compare them with one another.

<div class="tip emoji" markdown="1">
💡 **A Quick Refresher on Z-Scores:**  
A z-score of "0" would indicate the assessed value is equal to the mean of all the other values in the dataset.

A z-score of "2" would indicate the assessed value is 2 standard deviation units away from the mean.

When the values in a dataset follow a normal distribution, a z-score greater than 2.5 is rare. 

If a dataset contained 100 values, we would only expect one value to have a z-score greater than 2.5. 

A z-score greater than 3 is **extremely** unlikely. In fact, in a lot of data analysis and machine learning scenarios, a value with a z-score greater than 3 is considered an "[outlier](http://statisticshelper.com/what-is-a-z-score-what-they-mean)". i.e. the value is removed from the analysis because it is considered so rare that it must be an "anomaly". *Let that sink in.*
</div>

We can calculate z-scores for all key parameters, discussed in the previous articles, which will allow us to compare the 2020 results with the previous 5 elections, to quickly identify the 2020 values that look unlikely.

The most important parameters are:

T/DR | Total Democrat votes divided by Democrat registrations
DVI | Democrat Vote Increase (from previous election)
RVI | Republican Vote Increase (from previous election)
TI/TRI | Total Vote Increase divided by Total Registration Increase
TV | Total Votes
TR | Total Registrations

Not all states provide registration data, so we can't calculate the T/DR for all states. However we *can* calculate the DVI for every state, so we will look at the zDVI score first.

## Looking at Democrat Vote Increases: The DVI Z-Score (zDVI)

We calculated the DVI for all counties in all states, and then determined the z-score for each DVI in 2020 (in the zDVI column). We then filtered the list to only display the counties with a DVI z-score greater than 2.5.

<div class="tip emoji" markdown="1">
💡 **TIP:**  
A z-score of 2.5 will put the value in the 99% percentile. In other words we should only expect to see such values once in every 100 elections.

A z-score greater than 3 is **extremely** unlikely. (Remember, in many data analysis scenarios these values are considered so rare that they are called "outliers" and removed from the dataset.)
</div>

There are over **388** counties across **37** states that have a zDVI score greater than 2.5! So we had to display the images on their own page:

[Show Full List of zDVI Values Greater than 2.5, By State](/trend-analysis/zdvi-trend-anomalies/){:.button}

<div class="important emoji" markdown="1">
🤯 For comparison, if we compare the 2016 election results with the previous 4 elections, we only get *five* counties with a zDVI greater than 2.5. In 2020, there was suddenly 388!
</div>

**Summary:**

These are the states with the most counties with a zDVI score greater than 2.5:

- Texas has 73
- Georgia has 51
- Kansas has 27
- Washington has 22
- Oklahoma has 21
- Tennessee has 21
- Kentucky has 18
- Oregon has 17
- New York has 11
- California has 11
- Utah has 11
- Colorado has 10
- Arizona has 10
- South Carolina has 9
- Alabama has 9
- Florida has 7
- Virginia has 6

A state that is notably missing is Pennsylvania... which makes one wonder how much worse the states in the list above are!

**Observations:**

- A lot of the counties with a zDVI score greater than 2.5 are Republican strongholds. They have consistently voted Republican since (at least) the year 2000. Does it really make any sense that the Democrat Vote Increase (DVI) would set new records in 2020, in such counties?
- Whenever a county has a large zRVI (Republican) score it is often eclipsed by an even larger zDVI score, especially in the larger counties. ([Coincidence?](/in-detail/battle-for-largest-counties/))
- The fact that so many counties across so many states have abnormally high z-score values is *troubling* (to say the least).


## A Look at Democrat Voter Turnout Rates: The T/DR Z-Score (zTDR)

As we have mentioned previously, we can only calculate the T/DR ratio (i.e the Democrat votes divided by the Democrat registrations), if a state provides registration data by party.

When we *do* have access to the registration data, we believe an "abnormal" T/DR value is the most powerful indicator of suspicious activity within a county.

There are over **331** counties across **17** states that have a zTDR score greater than 2.5, so again we had to display the images on their own page:

[Show Full List of zTDR Values Greater than 2.5, By State](/trend-analysis/ztdr-trend-anomalies/){:.button}

<div class="important emoji" markdown="1">
🤯 Again, for comparison, if we compare the 2016 election results with the previous four elections, we only get *thirteen* counties with a zTDR greater than 2.5. In 2020 it jumped to 331!
</div>

**Summary:**

These are the states with the most counties with a zTDR score greater than 2.5:

- North Carolina has 67
- Oklahoma has 49
- Florida has 34
- Louisiana has 33
- Pennsylvania has 24
- Oregon has 21
- Kentucky has 15
- Maryland has 10
- Massachusetts has 9
- Arizona has 7


**Observations:**

- What happened in Oklahoma, Oregon, Nebraska, Louisiana, Kentucky and North Carolina? All these counties are Republican strongholds!
- Many of the counties with high zTDR scores are different to the counties with high zDVI scores. This is concerning because that does not leave us with a lot of counties with low z-scores.
- Only 17 states provide full registration data by party. Imagine how many more counties with zTDR values greater than 2.5 there would be, if we had registration data for all 50 states.

## 2016 Comparison

Here are the **17** counties that have either their zDVI *or* zTDR value greater than 2.5 at the 2016 election:

{% include large_image url="/images/trend-analysis/trends/zScoreGreaterThan2_5For2016.png" maxWidth="1400" %}

In 2020, the number of counties with either a zDVI *or* zTDR value greater than 2.5 ballooned to ***647***. (As we noted above, [388 counties](/trend-analysis/zdvi-trend-anomalies/) have a zDVI greater than 2.5 and [331 counties](/trend-analysis/ztdr-trend-anomalies/) have a zTDR greater than 2.5.)

**Observations:**

- It is remarkable that there is so little overlap between counties with large zDVI and zTDR values
- Unsurprisingly, many of the "anomalous" counties in 2016 are very large counties (based on the total votes shown in the "TV 20" column)
- Orange County, CA, voted Democrat for the first time in a long time in 2016, after a huge Democrat surge (and an even bigger surge in 2020)
- Miami-Dade, FL, had a large Democrat surge in 2016, which then completely collapsed in 2020. What happened? Why such a big change of heart?
- Massachusetts has large counties with **extremely** unlikely z-scores in 2016 😱, and even higher (much higher!) z-scores in 2020. (Yes, that is a big give away 😉)


## Full County Analysis by State

Here is the CSV file containing the complete analysis for **ALL** counties in **ALL** states. 

[Download Complete CSV](/trend-analysis/CountySummaryAnalysis.csv.zip){:.button}

You can use this CSV file to look at your county and compare it with other counties in your state.

Remember: any z-score, in any column, that is greater than 2.5 is suspicious and would warrant further investigation.

The table includes the following analyses:

- Winning Range analysis (Loc.20)
- Registration and Vote trend analysis (RVT category: N-R and N-D)
- 2012 and 2016 Negative DVI value categorization (DVI-Cat)
- DVI# for 2008 and 2020 (Comparing Obama's results with Biden's)
- Z-scores of all key parameters

## Summary

The z-score is an impartial measurement for classifying values within a dataset.

During a normal election we would only expect to find a handful of counties with high z-score values, above 2.5, like in 2016. Here's the comparison again:

<table>
    <thead>
        <tr>
            <th>Election</th>
            <th>Based on Democrat Vote Increase</th>
            <th>Based on Democrat Turnout</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>2016</th>
            <td>5 counties over 2.5</td>
            <td>13 counties over 2.5</td>
        </tr>
        <tr>
            <th class="red">2020</th>
            <td class="red">388 counties over 2.5</td>
            <td class="red">331 counties over 2.5</td>
        </tr>
    </tbody>
</table>

We only focused on the DVI and T/DR values in this article, however other important parameters, such as RVI, TR, and TV, all display troubling z-score values as well.

The 2020 election had **hundreds** of counties with **extremely high** z-score values. The *quantity and magnitude* of the z-scores indicate the results are **extremely unlikely**. 

(Just think of the odds that the same person would be struck by lightening 3 years in a row, at exactly the same location, on the same date, at exactly the same time, wearing exactly the same clothes, with Mars, Venus and Neptune being aligned, during a total solar eclipse, without a single cloud in the sky. Yes... extremely unlikely!)

These unlikely values are consistent with our other findings and point to the fact that more needs to be done to investigate the 2020 election.

**The data speaks for itself.** 

At this stage of the analysis the evidence is overwhelming. We'd even venture to say that anyone refusing to take this seriously and allow independent audits of the election is either willfully blind or complicit. We call on all officials and state representatives to look openly and transparently at the results and allow the US public to verify that the election was run honestly and securely.

----

Stay tuned for our next article on *Down Ballot Analysis*, where we look at interesting statistical findings on how the Presidential race results compared to the other races in the same election.

----

{% include article_series from="/trend-analysis/" %}