---
title: List of Unlikely z-Score DVI Values Across All Counties in 2020 Election
meta_desc: 
parent_page: /trend-analysis/unlikely-z-scores/
last_updated: 15 Nov 2021
wide_layout: false
---

From our article on *[Unlikely z-Score values](/trend-analysis/unlikely-z-scores/)*, here is the full list of counties (across the whole country) where, in 2020, the z-Score value for the Democrat Vote Increase (zDVI) is greater than 2.5.

<div class="tip emoji" markdown="1">
💡 **TIP:**  
All these counties are highly suspicious and would warrant further investigations. 

Values over 2.5 | Rare (typically only 1 in 100) | 🧐
Values over 3 | *Extremely* unlikely | 😲
Values over 4 | *"Super extremely"* unlikely | 😱

In other words, values over 4 should be ***super extremely rare***, yet there are **79** counties across 17 states with a zDVI greater than 4!

- Maricopa County, Arizona is one of them
- Georgia has 11
- Tennessee has 11
- Texas has the most counties at 13
</div>

{% include large_image url="/images/trend-analysis/trends/zDviTrend1.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend2.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend3.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend4.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend5.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend6.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend7.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend8.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend9.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend10.png" maxWidth="1400" %}

{% include large_image url="/images/trend-analysis/trends/zDviTrend11.png" maxWidth="1400" %}


----

{% include article_series from="/trend-analysis/" %}