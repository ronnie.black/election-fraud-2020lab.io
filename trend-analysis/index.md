---
title: Identify Electoral Fraud Using Trend Analysis
breadcrumb: "[Follow the Data](#colophon) › "
meta_desc: Diving deeper into the unusual trends and statistics discovered in the 2020 election.
last_updated: 7 Oct 21
articles:
  - /in-detail/bellwether-states-counties-2020/
  - /in-detail/battle-for-largest-counties/
  - /trend-analysis/historical-voting-rate-trends/
  - /trend-analysis/3-historical-county-trends/
  - /trend-analysis/historical-county-analysis-registration-data/
  - /trend-analysis/further-registration-trends/
  - /trend-analysis/large-democrat-vote-increases/
  - /trend-analysis/unlikely-z-scores/
  - /trend-analysis/down-ballot-election-analysis/
  - /trend-analysis/voter-roll-analysis/
  - /trend-analysis/art-of-the-steal/
comment_intro: Did you find these articles helpful? Did you dig in and review the data for your county or state? What did you find? Let us know in the comments below.
---

We will be publishing a series of articles over the coming days which will explain how to identify fraud at any election, by looking at and analyzing the data for yourself. This follows on from the trend analysis and heat maps done by [Seth Keshel](/seth-keshel-reports/), which we cover extensively on this site.

Some parts get a bit technical, but we'll do our best to break it down in simple terms with clear examples.

By the time you finish reading this series, *you* the reader will have the necessary knowledge to apply these simple techniques to discover anomalies and fraud on your own.

{:.highlight}
> You will also come to the realization that ***all*** the proof you need to ascertain the legitimacy of the 2020 election results is **already** in the data.

Find out *how* and *why* in the following articles.


## The First Rule of Data Analysis

The first and most important concept of any data analysis is understanding the importance of *trends* and knowing how to find them.

{:.highlight}
> Remember: **The trend is your friend!**
>
> As you follow the next few pages, keep an eye out for statistics that continually move in a similar direction or pattern, and any others that break or "buck" this pattern.

When it comes to analyzing election data, there are several different "trend" scenarios to consider:

- Historical General Election trends within a county
- Historical "Down Ballot" race trends within a county
- Historical registration trends within a county
- County trends within a state
- "Votes / Registrations" ratio trends by age within a county
- Historical "Voting Rates" for each state

The questions you need to be constantly asking yourself are:

- Is there a trend hidden in the data? (Can you identify one?)
- Does the trend make sense?
- Does it manifest itself in other data sets?
- Is the shape of the trend "flat", "increasing" or "decreasing"?
- Can we find correlations between different trends?
- Can we find a good, rational explanation for a broken trend (i.e. a change in direction)?
- Can the trends be categorized across time, counties and states?

Before we dive into the details it is important to get an understanding of the (simpler) "high level" trends first.

It is always best to first grasp the overall dynamics of the system under analysis, before diving into the details. And even when you are deep in an analysis, always keep **one** eye on the big picture.


## Articles

The simplest (yet possibly the most important) trends to start with are covered in our first three articles: 
- *[The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/)*
- *[The Battle of the Largest Counties](/in-detail/battle-for-largest-counties/)*
- *[The Curious Case of the 2020 “Voting Rate” Blowouts](/trend-analysis/historical-voting-rate-trends/)*

We recommend reading each of these and completing the associated tasks. The tasks will help you attain a deeper affinity regarding the importance of trends within data analysis. These overview articles will make the subsequent detailed trend analysis easier to comprehend.

<!--
EDITOR NOTE:
To add/remove from the list of articles, see the list of URLs at the top of this page. 
To modify titles and descriptions, these are located in the header of each individual article.
Changing these will update the list here as well as the list at the bottom of each article. 
-->

{% include article_series from="/trend-analysis/" title="Articles In This Series" %}


## Conclusion

We have looked at several different trend analysis techniques to help people analyse the data from the 2020 General Election for themselves.

Each of these techniques is sufficient, on its own, to raise serious doubts, enough to warrant further investigations into the 2020 General Election results.

{:.highlight}
> We believe that when combined, these techniques form a "[Nomological network](https://en.wikipedia.org/wiki/Nomological_network)", and provide a [convergence of evidence](https://en.wikipedia.org/wiki/Consilience), that make the proof of fraud *irrefutable*.

The *[The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/)* article should be proof-enough. We hope you can understand the sheer unlikelihood of 21 bellwether counties getting it wrong out of 22. (Not to mention that Clallam County's results look dubious enough that it's possible that *none* of the bellwether counties truly voted for Biden.) 

*Could it be, the bellwether counties actually got it right and that something else went horribly wrong?*

We hope you'll absorb this information and start demanding satisfying answers to these questions.

{:.caution.emoji style="font-size: 100%"}
⚠️ Would you ignore a canary warning in a coal mine?  
Ignore the bellwethers at your own risk.


**Further Reading:**

- [10 Indisputable Facts About The 2020 Election](https://www.westernjournal.com/gen-flynn-exclusive-10-indisputable-facts-2020-election-argue-audits/)
- [Seth Keshel's Reports](/seth-keshel-reports/)


<div class="info" markdown="1">
*If you find these articles meaningful and convincing, please share them with friends and relatives. The more people see for themselves that something is wrong, the more we will be able to hold elected officials accountable.*

*Feel free to forward a link to your elected officials as well.*
</div>

## Inquiries

If you need assistance, clarification, or would like to report an error, please read our [Election Data Inquiries](/trend-analysis/inquiries/) page first. Then create an issue on our [GitLab forum](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues) to initiate a conversation.

