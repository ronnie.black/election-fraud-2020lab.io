---
title: List of Trend-Bucking Counties in 2020 Election
meta_desc: 
parent_page: /trend-analysis/further-registration-trends/
last_updated: 31 Oct 2021
wide_layout: false
---

From our article on *[The Counties Where Votes and Party Registrations Don’t Align](/trend-analysis/further-registration-trends/)*, here is the full list of counties where, in 2020, the party shift in votes did *not* match the party shift in registrations.

This is where RVT 2020 category starts with a "N".

{:.tip.emoji}
💡 **TIP:** 
Keep an eye out for the categories that have one or two stars!


{% include large_image url="/images/trend-analysis/trends/PGapTrend1.png" maxWidth="900" %}

<div class="hint" markdown="1">
**Observations:**

Colorado: In 2016 all the counties were Y-R, but in 2020 they were nearly all N-R or N-D.
</div>

{% include large_image url="/images/trend-analysis/trends/PGapTrend2.png" maxWidth="900" %}


{% include large_image url="/images/trend-analysis/trends/PGapTrend3.png" maxWidth="900" %}

<div class="hint" markdown="1">
**Observations:**

Kentucky: In 2016 all the counties were Y-R, but in 2020 they were all N-R! (and many of them have two stars!!)
</div>

{% include large_image url="/images/trend-analysis/trends/PGapTrend4.png" maxWidth="900" %}

<div class="hint" markdown="1">
**Observations:**

Massachusetts: Largest counties have had more Republican registrations than Democrats. But it does not translate into a greater increase in Republican votes over Democrats...
</div>

{% include large_image url="/images/trend-analysis/trends/PGapTrend5.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/PGapTrend6.png" maxWidth="900" %}

<div class="hint" markdown="1">
**Observations:**

New York and North Carolina are trying hard to vote more Republican. To no avail...

</div>

{% include large_image url="/images/trend-analysis/trends/PGapTrend7.png" maxWidth="900" %}

<div class="hint" markdown="1">
**Observations:**

Oklahoma! Pennsylvania! West Virginia!

</div>

----

{% include article_series from="/trend-analysis/" %}

